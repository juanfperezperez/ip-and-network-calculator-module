#!/bin/sh
echo "Addressing Linux dependencies"
echo "Adding Microsoft's GPG key"
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg

echo "Adding Microsoft's Package Source"
sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-xenial-prod xenial main" > /etc/apt/sources.list.d/dotnetdev.list'

echo "Running 'apt-get update'"
apt-get -qq update > /dev/null
echo "Installing Node.js"
apt-get -qq install nodejs > /dev/null
echo "Installing NPM"
apt-get -qq install npm > /dev/null
echo "Installing Git"
apt-get -qq install git > /dev/null
echo "Installing .Net SDK v2.1.4"
apt-get -qq install dotnet-sdk-2.1.4 > /dev/null
echo "Creating symlink node for nodejs"
ln -s "$(which nodejs)" /usr/bin/node
echo "Installing GitBook CLI"
npm install --silent gitbook-cli -g
echo "Installing latest GitBook version"
gitbook fetch latest
gitbook install
echo "Done addressing Linux dependencies"
