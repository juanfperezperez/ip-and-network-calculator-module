$ProgressPreference = 'SilentlyContinue'
$ErrorActionPreference = 'Stop'
$RequiredProviders = @('NuGet','PowerShellGet')

$RequiredModules = @('InvokeBuild','PowerShellGet')
if((Get-PSRepository -Name 'PSGallery' -ErrorAction SilentlyContinue) -eq $null){
    Write-Host -ForegroundColor Gray -Object "Registering PSRepository: PSGallery"
    Register-PSRepository -Default -InstallationPolicy Trusted
    Write-Host -ForegroundColor Gray -Object "Registered PSRepository: PSGallery"
}
foreach ($RequiredProvider in $RequiredProviders) {
    if($RequiredProvider -notin (Get-PackageProvider | Select-Object -ExpandProperty Name)){
        Write-Host -ForegroundColor Gray -Object "Installing Package Provider: $RequiredProvider"
        Install-PackageProvider -Name $RequiredProvider -Force -ForceBootstrap -Scope CurrentUser | Out-Null
        Write-Host -ForegroundColor Gray -Object "Installed Package Provider: $RequiredProvider"
    }
}
foreach ($RequiredModule in $RequiredModules) {
    if($RequiredModule -notin (Get-Module -ListAvailable | Select-Object -ExpandProperty Name)){
        Write-Host -ForegroundColor Gray -Object "Installing Module: $RequiredModule"
        Install-Module -Name $RequiredModule -Force -Scope CurrentUser
        Write-Host -ForegroundColor Gray -Object "Installed Module: $RequiredModule"
    }
}
Import-Module -Name $RequiredModules -Force