param (
    [ValidateSet(
        'None','Default','Passed','Failed','Pending','Skipped','Inconclusive',
        'Describe','Context','Summary','Header','All','Fails'
    )]
    [string[]]$TestDisplayOption = @('Describe','Failed','Skipped')
)

Enter-Build {
    # Save, or Install module here
    $RequiredProviders = @('NuGet','PowerShellGet')
    $RequiredModules = @('BuildHelpers','Pester','PSDeploy','PSScriptAnalyzer','PowerShellGet')
    Write-Build Gray "Processing Dependencies"
    Write-Build Gray "Processing Dependency: PSGallery [Repository]"    
    if((Get-PSRepository -Name 'PSGallery' -ErrorAction SilentlyContinue) -eq $null){
        Write-Build Gray "Registering PSRepository: PSGallery"
        Register-PSRepository -Default -InstallationPolicy Trusted
        Write-Build Gray "Registered PSRepository: PSGallery"
    }
    foreach ($RequiredProvider in $RequiredProviders) {
        Write-Build Gray "Processing Dependency: $RequiredProvider [PackageProvider]" 
        if($RequiredProvider -notin (Get-PackageProvider | Select-Object -ExpandProperty Name)){
            Write-Build Gray "Installing Package Provider: $RequiredProvider"
            Install-PackageProvider -Name $RequiredProvider -Force -ForceBootstrap -Scope CurrentUser | Out-Null
            Write-Build Gray "Installed Package Provider: $RequiredProvider"
        }
    }
    foreach ($RequiredModule in $RequiredModules) {
        Write-Build Gray "Processing Dependency: $RequiredModule [Module]" 
        if($RequiredModule -notin (Get-Module -ListAvailable | Select-Object -ExpandProperty Name)){
            Write-Build Gray "Installing Module: $RequiredModule"
            Install-Module -Name $RequiredModule -Force -Scope CurrentUser
            Write-Build Gray "Installed Module: $RequiredModule"
        }
    }
    Import-Module -Name $RequiredModules -Force | ForEach-Object {Write-Build Gray "Imported Module: $($_.Name) $($_.Version.ToString())"}

    #Set Variables
    Write-Build Gray "Setting Build Variables"
    Set-BuildEnvironment -BuildOutput '.build' -Force -ErrorAction Stop
    $DocsSource        = [System.IO.Path]::Combine([string[]]@($env:BHProjectPath, 'docs'))
    $Artifacts         = [System.IO.Path]::Combine([string[]]@($env:BHBuildOutput, 'Artifacts'))
    $ModuleBuildDir = [System.IO.Path]::Combine([string[]]@($env:BHBuildOutput, 'Module'))
    $ModuleBuildOutput = [System.IO.Path]::Combine([string[]]@($ModuleBuildDir,$env:BHProjectName))
    $DocsBuildOutput   = [System.IO.Path]::Combine([string[]]@($Artifacts, 'docs'))

    $VolatileRepoName = ([guid]::NewGuid().ToString())

    $nodePresent    = (Get-Command -Name node,nodejs -ErrorAction SilentlyContinue) -ne $null
    $GitbookPresent = (Get-Command -Name gitbook -ErrorAction SilentlyContinue) -ne $null
}

Task . Default

Task Default Test,Build,CreateArtifacts

Task FullPipeline Test,Build,CreateArtifacts,Deploy

Task Deploy DeployDocs_GitLabPages,DeployModuleToPSGallery

Task Build CleanBuildDirectoryStructure,BuildModule

Task TestAndBuild Test,Build

Task BuildAndCreateArtifacts Build,CreateArtifacts

Task CreateArtifacts PackageZip,PackageNuget,BuildDocumentation

Task BuildDirectoryStructure {
    $BuildDirectories = @($env:BHBuildOutput,$ModuleBuildDir,$Artifacts)
    foreach($dir in $BuildDirectories){
        if(!(Test-Path -Path $dir -PathType Container)){
            New-Item -Path $dir -ItemType Directory | Out-Null
        }
        assert (Test-Path -Path $dir -PathType Container) "Missing build directory: $dir"
    }
}

Task CleanBuildDirectoryStructure {
    if(Test-Path -Path $env:BHBuildOutput -PathType Container){
        Remove-Item -Path $env:BHBuildOutput -Recurse -Force
    }
    assert (!(Test-Path -Path $env:BHBuildOutput -PathType Container)) "failed build directory cleanup: $env:BHBuildOutput"
},BuildDirectoryStructure

Task Test {
    $testResults = Invoke-Pester -PassThru -Show $TestDisplayOption
    assert ($testResults.FailedCount -eq 0) "There were $($testResults.FailedCount) failed test(s)"
}

Task BuildModule {
    Set-ModuleFunctions -Name $env:BHPSModuleManifest -FunctionsToExport 'New-IPCalcNetwork'
    Get-Item -Path $env:BHModulePath -ErrorAction Stop | Copy-Item -Destination $ModuleBuildOutput -Recurse -ErrorAction Stop
}

Task BuildDocumentation {
    Copy-Item -Path ([System.IO.Path]::Combine($env:BHProjectPath,'README.md')) -Destination $DocsSource -Force -ErrorAction Stop
    assert ($nodePresent -and $GitbookPresent) ("GitBook tools exist: $($nodePresent -and $GitbookPresent)")
    exec {gitbook build $DocsSource $DocsBuildOutput}
}

Task PackageZip {
    assert (Test-Path -Path $ModuleBuildOutput -PathType Container) "Missing module folder: $ModuleBuildOutput"
    $ModuleInfo = Get-Module -ListAvailable -Name $ModuleBuildOutput
    $ZipPackagePath = [System.IO.Path]::Combine([string[]]@($Artifacts, "$env:BHProjectName.$($ModuleInfo.Version.ToString()).zip"))
    if (Test-Path -Path $ZipPackagePath -PathType Leaf) {
        Remove-Item -Path $ZipPackagePath -Force
    }
    Compress-Archive -Path $ModuleBuildOutput -DestinationPath $ZipPackagePath -ErrorAction Stop
    "Created Zip File: $($ZipPackagePath)" | Write-Host -ForegroundColor Gray
}
Task PackageNuget {
    $VolatileRepoParams = @{
        "Name" = $VolatileRepoName
        "SourceLocation" = $Artifacts
        "PublishLocation" = $Artifacts
        "InstallationPolicy" = "Untrusted"
        "PackageManagementProvider" = "NuGet"
    }
    if(! (Test-Path -Path $Artifacts -PathType Container)){New-Item -Path $Artifacts -ItemType Directory | Out-Null}
    Write-Build Gray "Registering temporary module repository: $VolatileRepoName"
    Register-PSRepository @VolatileRepoParams

    assert ((Get-PSRepository -Name $VolatileRepoName -ErrorAction SilentlyContinue) -ne $null) "Volatile PSRepository does not exist: $VolatileRepoName"
    assert (Test-Path -Path $ModuleBuildOutput -PathType Container) "Missing module folder: $ModuleBuildOutput"
    Publish-Module -Path $ModuleBuildOutput -Repository $VolatileRepoName

    Write-Build Gray "Unregistering temporary module repository: $VolatileRepoName"
    Unregister-PSRepository -Name $VolatileRepoName
}
Task DeployDocs_GitLabPages {
    $GitLabPagesDir = [System.IO.Path]::Combine('.','public')
    if(!(Test-Path -Path $GitLabPagesDir -PathType Container)){
        New-Item -Path $GitLabPagesDir -ItemType Directory | Out-Null
    }
    assert (Test-Path -Path $GitLabPagesDir -PathType Container) "Documentation destination directory missing: $GitLabPagesDir"
    assert (Test-Path -Path $DocsBuildOutput -PathType Container) "Documentation source directory missing: $DocsBuildOutput"
    Get-ChildItem -Path $DocsBuildOutput | Copy-Item -Destination $GitLabPagesDir -Recurse
}
Task DeployModuleToPSGallery {
    assert ((Get-PSRepository -Name 'PSGallery' -ErrorAction SilentlyContinue) -ne $null) "Missing repository: PSGallery"
    assert ($env:PSGallery_NuGetApiKey -ne $null) "Missing NuGetApiKey"
    Publish-Module -Path $ModuleBuildOutput -Repository 'PSGallery' -NuGetApiKey $env:PSGallery_NuGetApiKey
}

Task ApplyTags {
    [System.Version]$PublishedVersion = try{
        Find-Module -Name $env:BHProjectName -Repository PSGallery -ErrorAction Stop |
            Select-Object -ExpandProperty Version -ErrorAction Stop
    }
    catch{
        [System.Version]'0.0.0'
    }
    Write-Build Gray "Published version: $($PublishedVersion.ToString())"
    
    $latestRelease = Invoke-Git tag -l v*-release* |
        Where-Object {$_ -match '^v(\d+\.){2,3}\d-release(-candidate)?$'} |
        ForEach-Object {$_ -replace '^v((\d+\.){2,3}\d)-release(-candidate)?$','$1'} |
        ForEach-Object {[System.Version]$_} | Sort-Object -Descending | Select-Object -First 1
    
    if($null -eq $latestRelease){$latestRelease = [System.Version]'0.0.0'}
    Write-Build Gray "Latest Release Version: $($latestRelease.ToString())"
    
    [System.Version]$CurrentVersion = try{Get-Metadata -Path $env:BHPSModuleManifest -PropertyName ModuleVersion}
    catch{[System.Version]'0.0.0'}
    Write-Build Gray "Current Version: $($CurrentVersion.ToString())"
    
    $isRelease = (($CurrentVersion -gt $latestRelease) -and ($CurrentVersion -gt $PublishedVersion))

    if($isRelease){
        $tag = if($env:BHBranchName -eq 'master'){
            'v{0}-release' -f $CurrentVersion.ToString()
        }
        else{
            'v{0}-release-candidate' -f $CurrentVersion.ToString()
        }
        exec {git tag $tag}
        exec {git push --tags}
    }
    else {
        Write-Build Gray "Current build is not a new release"
    }
}