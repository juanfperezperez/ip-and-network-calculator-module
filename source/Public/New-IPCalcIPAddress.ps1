<#
.SYNOPSIS
New-IPCalcIPAddress returns an IPAddress object for the corresponding input.

.DESCRIPTION
New-IPCalcIPAddress returns an IPAddress object for the corresponding input. It
accepts an IP address string in dotted decimal notation (0.0.0.0) or an array of
four bytes ([byte[]]@(0,0,0,0)).

.PARAMETER IPAddressString
IP address string in dotted decimal notation (0.0.0.0)

.PARAMETER IPAddressBytes
Array of four bytes ([byte[]]@(0,0,0,0))

.EXAMPLE
New-IPCalcIPAddress -IPAddressString 192.168.0.1


Address            : 16820416
AddressFamily      : InterNetwork
ScopeId            :
IsIPv6Multicast    : False
IsIPv6LinkLocal    : False
IsIPv6SiteLocal    : False
IsIPv6Teredo       : False
IsIPv4MappedToIPv6 : False
IPAddressToString  : 192.168.0.1

.EXAMPLE
New-IPCalcIPAddress -IPAddressBytes [byte[]]@(192,168,0,1)


Address            : 16820416
AddressFamily      : InterNetwork
ScopeId            :
IsIPv6Multicast    : False
IsIPv6LinkLocal    : False
IsIPv6SiteLocal    : False
IsIPv6Teredo       : False
IsIPv4MappedToIPv6 : False
IPAddressToString  : 192.168.0.1

.INPUTS
System.String

System.Byte[]

.OUTPUTS
System.Net.IPAddress

.NOTES
This function is a wrapper around the System.Net.IPAddress class constructor
and limits it to the creation of IPv4 Addresses only.
#>
function New-IPCalcIPAddress {
    [CmdletBinding(DefaultParameterSetName='String',PositionalBinding=$false)]
    param (
        [Parameter(Position=0,ParameterSetName='String',Mandatory=$true)]
        [ValidateScript({
            if($_ -notmatch '^([01]?\d?\d|2[0-4]\d|25[0-5])\.([01]?\d?\d|2[0-4]\d|25[0-5])\.([01]?\d?\d|2[0-4]\d|25[0-5])\.([01]?\d?\d|2[0-4]\d|25[0-5])$'){
                throw "String $_ was not a valid IP address"
            }
            else{
                $true
            }
        })]
        [string]$IPAddressString,
        [Parameter(Position=0,ParameterSetName='ByteArr',Mandatory=$true)]
        [ValidateRange(0,255)]
        [ValidateCount(4,4)]
        [byte[]]$IPAddressBytes
    )
    
    begin {
    }
    process {
        switch ($PSCmdlet.ParameterSetName) {
            'String' { [ipaddress]::Parse($IPAddressString) }
            'ByteArr' { New-Object -TypeName ipaddress -ArgumentList (,$IPAddressBytes) }
        }
    }
    end {
    }
}