<#
.SYNOPSIS
New-IPCalcNetwork returns an object that represents a network.

.DESCRIPTION
New-IPCalcNetwork returns an object that represents a network. The resulting object will
contain the IP address identifying the network (AKA the subnet ID), the subnet
mask for the network, and the mask length (AKA CIDR suffix).

.PARAMETER NetworkID
IP address string in dotted decimal notation (0.0.0.0) of an IP address in the
network to be returned. If this is not the actual network ID, the function will
calculate the correct IP and return it as part of the resulting object.

.PARAMETER NetworkMask
Network mask string in dotted decimal notation (0.0.0.0) of the network to be
returned. If this is not a valid subnet mask, the function will error. 

.PARAMETER NetworkMaskLength
Integer in the range 0 - 32 representing the mask length of the network to be
returned.

.EXAMPLE
    New-IPCalcNetwork -NetworkID 192.168.1.0 -NetworkMask 255.255.255.0
    Returns an object representing the new network with the provided IP address
    and Mask

.EXAMPLE
    New-IPCalcNetwork -NetworkID 192.168.1.0 -NetworkMaskLength 24
    Returns an object representing the new network with the provided IP address
    and Mask of the specified length

.INPUTS
System.String

System.Int32

.OUTPUTS
System.Management.Automation.PSCustomObject

.NOTES
This function is intended to be used as a tool for network planning. It should
help planners quickly split networks and determine if a particular IP address
belongs to that network.

#>
function New-IPCalcNetwork {
    [CmdletBinding(DefaultParameterSetName='NetworkMask',PositionalBinding=$false)]
    param (
        [Parameter(Position=0,ParameterSetName='NetworkMask',Mandatory=$true)]
        [Parameter(Position=0,ParameterSetName='NetworkMaskLength',Mandatory=$true)]
        [ValidateScript({
            if($_ -notmatch '^([01]?\d?\d|2[0-4]\d|25[0-5])\.([01]?\d?\d|2[0-4]\d|25[0-5])\.([01]?\d?\d|2[0-4]\d|25[0-5])\.([01]?\d?\d|2[0-4]\d|25[0-5])$'){
                throw "String $_ was not a valid IP address"
            }
            else{
                $true
            }
        })]
        [string]$NetworkID,
        [Parameter(Position=1,ParameterSetName='NetworkMask',Mandatory=$true)]
        [ValidateScript({$_ -in $(GetValidMask)})]
        [string]$NetworkMask,
        [Parameter(Position=1,ParameterSetName='NetworkMaskLength',Mandatory=$true)]
        [ValidateRange(0,32)]
        [int]$NetworkMaskLength
    )
    
    begin {
    }
    
    process {
        $OBJMask = switch ($PSCmdlet.ParameterSetName){
            'NetworkMask' {
                [ipaddress]$NetworkMask
            }
            'NetworkMaskLength' {
                $(GetValidMask)[($NetworkMaskLength)]
            }
        }
        $NetworkObj = New-Object -TypeName psobject -Property @{
            ID = GetNetworkID -IPAddress $NetworkID -Mask $OBJMask
            MaskLength =  $(GetValidMask).IndexOf($OBJMask)
        } |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'Mask' -Value {$(GetValidMask)[$This.MaskLength]} |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'BroadcastAddress' -Value {GetNetworkBroadcast -IPAddress $this.ID -MaskLength $this.MaskLength} |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'AddressClass' -Value {GetIPAddressClass -IPAddress $this.ID} |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'IsPrivate' -Value {TestIPAddressIsPrivate -IPAddress $this.ID} |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'MaxAddressCount' -Value {GetMaxAddressCount -MaskLength $this.MaskLength} |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'MaxHostCount' -Value {GetMaxAddressCount -Usable -MaskLength $this.MaskLength} |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'MaxSubnetCount' -Value {GetMaxSubnetCount -MaskLength $this.MaskLength} |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'FirstHost' -Value {GetFirstNetworkAddress -IPAddress $this.ID -MaskLength $this.MaskLength} |
        Add-Member -PassThru -MemberType ScriptProperty -Name 'LastHost' -Value {GetLastNetworkAddress -IPAddress $this.ID -MaskLength $this.MaskLength} |
        Add-Member -PassThru -MemberType ScriptMethod   -Name 'ToString' -Value {'{0}/{1}' -f $this.ID.IPAddressToString,$this.MaskLength} -Force |
        Add-Member -PassThru -MemberType ScriptMethod   -Name 'Split' -Value {
            param([int]$SubnetCount)
            SplitNetwork -IPAddress $this.ID -MaskLength $this.MaskLength -SubnetCount $SubnetCount | ForEach-Object {
                New-IPCalcNetwork -NetworkID $_['IPAddress'] -NetworkMaskLength $_['MaskLength']
            }
        } |
        Add-Member -PassThru -MemberType ScriptMethod -Name 'ContainsIP' -Value {
            param([IPAddress]$IPAddress)
            TestNetworkContainsIP -IPAddress $this.ID -MaskLength $this.MaskLength -ChildIPAddress $IPAddress
        } 
        $NetworkObj.psobject.TypeNames.Insert(0,'IPCalcNetwork')
        $NetworkObj
    }
    
    end {
    }
}