function GetMaxAddressCount {
    [CmdletBinding(DefaultParameterSetName='Mask')]
    param(
        [Parameter(ParameterSetName='Mask')]
        [ValidateScript({$_ -in $(GetValidMask)})]
        [ipaddress]$Mask,
        [Parameter(ParameterSetName='MaskLength')]
        [ValidateRange(0,32)]
        [int]$MaskLength,
        [switch]$Usable
    )
    begin{}
    process{
        $Length = switch ($PSCmdlet.ParameterSetName) {
            Mask { $(GetValidMask).IndexOf($Mask) }
            MaskLength { $MaskLength }
        }
        $Total = [math]::Pow(2,(32-$Length))
        $result = if($Usable){ if($Total -le 2){0}else{$Total - 2} }else{$Total}
        $result
    }
    end{}
}