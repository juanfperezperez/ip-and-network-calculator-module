function TestIPAddressIsPrivate {
    [CmdletBinding()]
    param(
        [ipaddress]$IPAddress
    )
    begin{
        $privateRanges = @(
            [pscustomobject]@{IPAddress = [ipaddress]'10.0.0.0';Mask = [ipaddress]'255.0.0.0';DefaultSubnetMask = [ipaddress]'255.0.0.0';Class = 'A'}
            [pscustomobject]@{IPAddress = [ipaddress]'172.16.0.0';Mask = [ipaddress]'255.240.0.0';DefaultSubnetMask = [ipaddress]'255.255.0.0';Class = 'B'}
            [pscustomobject]@{IPAddress = [ipaddress]'192.168.0.0';Mask = [ipaddress]'255.255.0.0';DefaultSubnetMask = [ipaddress]'255.255.255.0';Class = 'C'}
        )
    }
    process{
        $isPrivate = $false
        foreach($range in $privateRanges){
            $isPrivate = $isPrivate -or (($IPAddress.Address -band $range.Mask.Address) -eq $range.IPAddress.Address)
        }
        if($isPrivate){
            Write-Verbose -Message "Is private IP range '$($range.Class)'"
        }
        if(($IPAddress.Address -band ([ipaddress]'255.255.0.0').Address) -eq ([IPAddress]'169.254.0.0').Address){
            Write-Verbose -Message "Is APIPA address"
        }
        $isPrivate
    }
    end{}
}