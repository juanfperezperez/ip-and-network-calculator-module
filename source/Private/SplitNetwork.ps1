function SplitNetwork {
    [CmdletBinding(DefaultParameterSetName='Mask')]
    param(
        [ipaddress]$IPAddress,
        [Parameter(ParameterSetName='Mask')]
        [ValidateScript({$_ -in $(GetValidMask)})]
        [ipaddress]$Mask,
        [Parameter(ParameterSetName='MaskLength')]
        [ValidateRange(0,32)]
        [int]$MaskLength,
        [int32]$SubnetCount
        )
    begin{
        #
    }
    process{
        try{
            $SNMask = switch ($PSCmdlet.ParameterSetName) {
                'Mask' { $Mask }
                'MaskLength' { $(GetValidMask)[$MaskLength] }
            }
            $MaxCount = GetMaxSubnetCount -Mask $SNMask
            $ParentNetworkID = GetNetworkID -IPAddress $IPAddress -Mask $SNMask
            if($SubnetCount -gt $MaxCount){throw "SubnetCount may not be greater than $MaxCount for mask $($SNMask.IPAddressToString)"}
            $bits = -1
            do {
                $bits++
                $splitCount = [math]::Pow(2,$bits)
            } until ($splitCount -ge $SubnetCount)
            $NewMask = $(GetValidMask).IndexOf($SNMask) + $bits
            $PartialSubnets = @(0..($splitCount-1)).ForEach({[ipaddress]($_ * [math]::Pow(2,(32-$NewMask)))})
            $subnets = $PartialSubnets.foreach({@{IPAddress = [ipaddress]($ParentNetworkID.Address -bor $_.Address); MaskLength = $NewMask}})
            $subnets
        } catch {
            $PSCmdlet.ThrowTerminatingError($_)
        }
    }
    end{}
}