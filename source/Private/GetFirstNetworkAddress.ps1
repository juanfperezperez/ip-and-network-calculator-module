function GetFirstNetworkAddress {
    [CmdletBinding(DefaultParameterSetName='Mask')]
    param(
        [ipaddress]$IPAddress,
        [Parameter(ParameterSetName='Mask')]
        [ValidateScript({$_ -in $(GetValidMask)})]
        [ipaddress]$Mask,
        [Parameter(ParameterSetName='MaskLength')]
        [ValidateRange(0,32)]
        [int]$MaskLength
    )
    begin{
        $firstAddressMask = [ipaddress]'0.0.0.1'
    }
    process{
        $SNMask = switch ($PSCmdlet.ParameterSetName) {
            'Mask' { $Mask }
            'MaskLength' { $(GetValidMask)[$MaskLength] }
        }
        $FirstAddress = [ipaddress]((GetNetworkID @PSBoundParameters).Address -bor $firstAddressMask.Address)
        $FirstAddress
    }
    end{}
}