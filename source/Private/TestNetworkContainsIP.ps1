function TestNetworkContainsIP {
    [CmdletBinding(DefaultParameterSetName='Mask')]
    param(
        [ipaddress]$IPAddress,
        [Parameter(ParameterSetName='Mask')]
        [ValidateScript({$_ -in $(GetValidMask)})]
        [ipaddress]$Mask,
        [Parameter(ParameterSetName='MaskLength')]
        [ValidateRange(0,32)]
        [int]$MaskLength,
        [ipaddress]$ChildIPAddress
    )
    begin{}
    process{
        $SNMask = switch ($PSCmdlet.ParameterSetName) {
            'Mask' { $Mask }
            'MaskLength' { $(GetValidMask)[$MaskLength] }
        }
        [bool]$result = (GetNetworkID -IPAddress $IPAddress -Mask $SNMask) -eq (GetNetworkID -IPAddress $ChildIPAddress -Mask $SNMask)
        $result
    }
    end{}

}