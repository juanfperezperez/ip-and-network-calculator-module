function GetMaxSubnetCount {
    [CmdletBinding(DefaultParameterSetName='Mask')]
    param(
        [Parameter(ParameterSetName='Mask')]
        [ValidateScript({$_ -in $(GetValidMask)})]
        [ipaddress]$Mask,
        [Parameter(ParameterSetName='MaskLength')]
        [ValidateRange(0,32)]
        [int]$MaskLength
    )
    begin{}
    process{
        $Length = switch ($PSCmdlet.ParameterSetName) {
            Mask { $(GetValidMask).IndexOf($Mask) }
            MaskLength { $MaskLength }
        }
        $Total = [math]::Max(1,[math]::Pow(2,(30-$Length)))
        $result = If($total -eq 1){0}else{$Total}
        $result
    }
    end{}
}