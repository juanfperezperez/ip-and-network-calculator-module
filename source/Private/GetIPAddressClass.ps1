function GetIPAddressClass {
    [CmdletBinding(DefaultParameterSetName='Calculate')]
    param(
        [Parameter(ParameterSetName='Calculate',Mandatory=$true)]
        [ValidateScript({if((([ipaddress]$_).Address -band ([ipaddress]'255.0.0.0').Address) -eq 0){throw "First octet may not be 0"}else{$true}})]
        [ipaddress]$IPAddress,
        [Parameter(ParameterSetName='List',Mandatory=$true)]
        [switch]$List
    )
    begin{
        $classRanges = @(
            [pscustomobject]@{IPAddress = [ipaddress]'0.0.0.0';Mask = [ipaddress]'255.0.0.0';DefaultSubnetMask = $null;Class = 'NotValid'}
            [pscustomobject]@{IPAddress = [ipaddress]'127.0.0.0';Mask = [ipaddress]'255.0.0.0';DefaultSubnetMask = [ipaddress]'255.0.0.0';Class = 'LoopBackAndDiagnostics'}
            [pscustomobject]@{IPAddress = [ipaddress]'0.0.0.0';Mask = [ipaddress]'128.0.0.0';DefaultSubnetMask = [ipaddress]'255.0.0.0';Class = 'A'}
            [pscustomobject]@{IPAddress = [ipaddress]'128.0.0.0';Mask = [ipaddress]'192.0.0.0';DefaultSubnetMask = [ipaddress]'255.255.0.0';Class = 'B'}
            [pscustomobject]@{IPAddress = [ipaddress]'192.0.0.0';Mask = [ipaddress]'224.0.0.0';DefaultSubnetMask = [ipaddress]'255.255.255.0';Class = 'C'}
            [pscustomobject]@{IPAddress = [ipaddress]'224.0.0.0';Mask = [ipaddress]'240.0.0.0';DefaultSubnetMask = $null;Class = 'D'}
            [pscustomobject]@{IPAddress = [ipaddress]'240.0.0.0';Mask = [ipaddress]'240.0.0.0';DefaultSubnetMask = $null;Class = 'E'}
        )
    }
    process{
        switch($PSCmdlet.ParameterSetName){
            'Calculate' {
                $classRange = -1
                do {
                    $classRange++
                    $class = $classRanges[$classRange].Class
                    
                } until ($classRanges[$classRange].IPAddress.Address -eq ($IPaddress.Address -band $classRanges[$classRange].Mask.Address))
                if(($IPAddress.Address -band ([ipaddress]'255.255.0.0').Address) -eq ([IPAddress]'169.254.0.0').Address){
                    Write-Verbose -Message "Is APIPA address"
                }
                $class
            }
            'List' {
                $classRanges[0..($classRanges.Count -1)]
            }
        }
    }
    end{}
}