function GetLastNetworkAddress {
    [CmdletBinding(DefaultParameterSetName='Mask')]
    param(
        [ipaddress]$IPAddress,
        [Parameter(ParameterSetName='Mask')]
        [ValidateScript({$_ -in $(GetValidMask)})]
        [ipaddress]$Mask,
        [Parameter(ParameterSetName='MaskLength')]
        [ValidateRange(0,32)]
        [int]$MaskLength
        )
    begin{
        $LastAddressMask = [ipaddress]'255.255.255.254'
    }
    process{
        $SNMask = switch ($PSCmdlet.ParameterSetName) {
            'Mask' { $Mask }
            'MaskLength' { $(GetValidMask)[$MaskLength] }
        }

        $LastAddress = [ipaddress]((GetNetworkBroadcast @PSBoundParameters).Address -band $LastAddressMask.Address)
        $LastAddress
    }
    end{}
}