function GetNetworkBroadcast {
    [CmdletBinding(DefaultParameterSetName='Mask')]
    param(
        [ipaddress]$IPAddress,
        [Parameter(ParameterSetName='Mask')]
        [ValidateScript({$_ -in $(GetValidMask)})]
        [ipaddress]$Mask,
        [Parameter(ParameterSetName='MaskLength')]
        [ValidateRange(0,32)]
        [int]$MaskLength
    )
    begin{}
    process{
        $SNMask = switch ($PSCmdlet.ParameterSetName) {
            'Mask' { $Mask }
            'MaskLength' { $(GetValidMask)[$MaskLength] }
        }
        $broadcast = [ipaddress]($(GetNetworkID @PSBoundParameters).Address -bor $(GetInverseIPAddress -IPAddress $SNMask).Address)
        $broadcast
    }
    end{}

}