function GetInverseIPAddress {
    [CmdletBinding()]
    param([ipaddress]$IPAddress)
    $inverse = [ipaddress]([ipaddress]::Broadcast.Address -bxor $IPAddress.Address)
    Write-Verbose "Original Binary: $($IPAddress.GetAddressBytes().ForEach({[convert]::ToString($_,2).PadLeft(8,'0')}) -join '.')"
    Write-Verbose "     New Binary: $($inverse.GetAddressBytes().ForEach({[convert]::ToString($_,2).PadLeft(8,'0')}) -join '.')"
    $inverse
}