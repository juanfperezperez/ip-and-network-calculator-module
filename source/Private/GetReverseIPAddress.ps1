function GetReverseIPAddress {
    [CmdletBinding()]
    param([ipaddress]$IPAddress)
    $Reverse = [ipaddress]([byte[]]($IPAddress.GetAddressBytes()[3..0]))
    Write-Verbose "Original Binary: $($IPAddress.GetAddressBytes().ForEach({[convert]::ToString($_,2).PadLeft(8,'0')}) -join '.')"
    Write-Verbose "     New Binary: $($Reverse.GetAddressBytes().ForEach({[convert]::ToString($_,2).PadLeft(8,'0')}) -join '.')"
    $Reverse
}