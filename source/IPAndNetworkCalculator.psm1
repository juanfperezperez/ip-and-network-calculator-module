$ModuleRoot = Split-Path -Path $MyInvocation.MyCommand.Path -Parent

$PublicFunctionsDir  = Join-Path -Path $ModuleRoot -ChildPath Public
$PrivateFunctionsDir = Join-Path -Path $ModuleRoot -ChildPath Private

foreach($FunctionFile in $(Get-ChildItem -Path $PrivateFunctionsDir)){
    . $FunctionFile.FullName
}

$PublicFunctionNames = @()
foreach($FunctionFile in $(Get-ChildItem -Path $PublicFunctionsDir)){
    . $FunctionFile.FullName
    $PublicFunctionNames += $FunctionFile.BaseName
}

Export-ModuleMember -Function $PublicFunctionNames