# IP And Network Calculator
This module can be used to help network planning by providing functions for
subnet calculations.
**Build Status**
+ GitLab CI: [![pipeline status](https://gitlab.com/juanfperezperez/ip-and-networking-module/badges/master/pipeline.svg)](https://gitlab.com/juanfperezperez/ip-and-networking-module/commits/master)

**Test Coverage**
+ GitLab CI: [![coverage report](https://gitlab.com/juanfperezperez/ip-and-networking-module/badges/master/coverage.svg)](https://gitlab.com/juanfperezperez/ip-and-networking-module/commits/master)

## Install

This module can be installed from the PowerShell Gallery.

``` PowerShell
Install-Module -Name IPAndNetworkCalculator
```
It can also be built from source. Once the project has been downloaded and extracted, the following code should build and import the module.

``` PowerShell
. .\build.init.ps1
Invoke-Build Build
```
> **NOTE:** The build script requires PowerShellGet to download and install the
build dependencies.

## Use

There are three operations currently supported:
1. Getting subnet information given an IP address and a subnet mask or mask
(CIDR) length via the `New-IPCalcNetwork` function,
1. splitting a network into two or more subnets via the `Split()` method, and
1. checking if an IP address belongs in a particular network via the `ContainsIP()` method.

```
PS > # Import the module
PS > Import-Module .\source\IPAndNetworkCalculator.psd1
PS >
PS > # create an object representing a subnet using the subnet mask
PS > New-IPCalcNetwork -NetworkID 192.168.1.1 -NetworkMask 255.255.255.0


ID               : 192.168.1.0
MaskLength       : 24
Mask             : 255.255.255.0
BroadcastAddress : 192.168.1.255
AddressClass     : C
IsPrivate        : True
MaxAddressCount  : 256
MaxHostCount     : 254
MaxSubnetCount   : 64
FirstHost        : 192.168.1.1
LastHost         : 192.168.1.254



PS > # create an object representing a subnet using CIDR notation
PS > New-IPCalcNetwork -NetworkID 192.168.2.1 -NetworkMaskLength 24


ID               : 192.168.2.0
MaskLength       : 24
Mask             : 255.255.255.0
BroadcastAddress : 192.168.2.255
AddressClass     : C
IsPrivate        : True
MaxAddressCount  : 256
MaxHostCount     : 254
MaxSubnetCount   : 64
FirstHost        : 192.168.2.1
LastHost         : 192.168.2.254



PS > # Split a network into 2 subnets.
PS > $NW = New-IPCalcNetwork -NetworkID 192.168.3.1 -NetworkMaskLength 24
PS > $NW.Split(2)


ID               : 192.168.3.0
MaskLength       : 25
Mask             : 255.255.255.128
BroadcastAddress : 192.168.3.127
AddressClass     : C
IsPrivate        : True
MaxAddressCount  : 128
MaxHostCount     : 126
MaxSubnetCount   : 32
FirstHost        : 192.168.3.1
LastHost         : 192.168.3.126

ID               : 192.168.3.128
MaskLength       : 25
Mask             : 255.255.255.128
BroadcastAddress : 192.168.3.255
AddressClass     : C
IsPrivate        : True
MaxAddressCount  : 128
MaxHostCount     : 126
MaxSubnetCount   : 32
FirstHost        : 192.168.3.129
LastHost         : 192.168.3.254



PS > # Check if an IP belongs in a network.
PS > $NW = New-IPCalcNetwork -NetworkID 192.168.4.1 -NetworkMaskLength 24
PS > $NW.ContainsIP('192.168.4.20')
True
PS > $NW.ContainsIP('192.168.5.20')
False
PS >
```
## Intended Audience

This module is intended to assist anyone who will need to perform IP calculations
such as determining the correct subnet ID given an IP address and a subnet mask.
The focus in mind while building this module was on network planners and students.

### Network Planners
In the context of this project a network planner is anyone who needs to perform
IP calculations in order to perform some task. This obviously includes network
administrators but can also expand to systems administrators and engineers. 

### Students
Often students are required to perform network calculations in order to complete
class work which does not directly relate to networking. Although, it is
important for students to understand subnetting and to develop that skill, this
should not keep them from completing their work. This module should serve as a
resource to help perform subnetting tasks as well a provide reference information.