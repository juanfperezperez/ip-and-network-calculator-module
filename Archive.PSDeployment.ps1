[CmdletBinding()]
param(
    [string]$SourcePath,
    [string]$DestinationPath

)
try{
    if (Test-Path -Path $DestinationPath -PathType Leaf) {
        Remove-Item -Path $DestinationPath -Force
    }
    Compress-Archive -Path $SourcePath -DestinationPath $DestinationPath -ErrorAction Stop
    "Created .zip File: $($DestinationPath)" | Write-Host -ForegroundColor Gray
}
catch{
    $PSCmdlet.ThrowTerminatingError($_)
}
