Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'GetFirstNetworkAddress'
    $TestTable = @(
        @{IPAddress = '10.0.0.0'; Mask = '255.255.255.0'; MaskLength = '24';FirstAddress = '10.0.0.1'}
        @{IPAddress = '10.0.0.0'; Mask = '255.255.255.128'; MaskLength = '25';FirstAddress = '10.0.0.1'}
        @{IPAddress = '10.0.0.128'; Mask = '255.255.255.128'; MaskLength = '25';FirstAddress = '10.0.0.129'}
        @{IPAddress = '10.0.0.0'; Mask = '255.255.254.0'; MaskLength = '23';FirstAddress = '10.0.0.1'}
        @{IPAddress = '10.0.1.0'; Mask = '255.255.254.0'; MaskLength = '23';FirstAddress = '10.0.0.1'}
        @{IPAddress = '10.0.2.0'; Mask = '255.255.255.0'; MaskLength = '24';FirstAddress = '10.0.2.1'}
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $ExpectedParameterList = @(
            'IPAddress','Mask','MaskLength','Verbose',
            'Debug','ErrorAction','WarningAction','InformationAction',
            'ErrorVariable','WarningVariable','InformationVariable',
            'OutVariable','OutBuffer','PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            it "has the correct default parameter set" {
                $Function.DefaultParameterSet | Should Be 'Mask'
            }
        }

        Context "ParameterSet: Mask" {
            $ParameterSetName = 'Mask'
            $ExpectedParameterSetParams = @(
                'IPAddress','Mask','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<IPAddress>/<Mask>: <FirstAddress>)" -TestCases $TestTable {
                param([ipaddress]$IPAddress,[ipaddress]$Mask,[int]$MaskLength,[ipaddress]$FirstAddress)
                {GetFirstNetworkAddress -IPAddress $IPAddress -Mask $Mask} | Should Not Throw
                GetFirstNetworkAddress -IPAddress $IPAddress -Mask $Mask| Should Be $FirstAddress
            }
        }
        Context "ParameterSet: MaskLength" {
            $ParameterSetName = 'MaskLength'
            $ExpectedParameterSetParams = @(
                'IPAddress','MaskLength','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<IPAddress>/<MaskLength>: <FirstAddress>)" -TestCases $TestTable {
                param([ipaddress]$IPAddress,[ipaddress]$Mask,[int]$MaskLength,[ipaddress]$FirstAddress)
                {GetFirstNetworkAddress -IPAddress $IPAddress -MaskLength $MaskLength} | Should Not Throw
                GetFirstNetworkAddress -IPAddress $IPAddress -MaskLength $MaskLength| Should Be $FirstAddress
            }
        }
    }
}