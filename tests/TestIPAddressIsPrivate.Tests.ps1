Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'TestIPAddressIsPrivate'
    $TestTable = @(
        @{IPAddress="10.239.82.176";IsPrivate=$True}
        @{IPAddress="10.112.180.177";IsPrivate=$True}
        @{IPAddress="10.214.93.35";IsPrivate=$True}
        @{IPAddress="10.48.141.236";IsPrivate=$True}
        @{IPAddress="10.248.88.239";IsPrivate=$True}
        @{IPAddress="10.176.47.160";IsPrivate=$True}
        @{IPAddress="10.18.134.134";IsPrivate=$True}
        @{IPAddress="10.139.151.41";IsPrivate=$True}
        @{IPAddress="10.110.193.39";IsPrivate=$True}
        @{IPAddress="10.136.54.206";IsPrivate=$True}
        @{IPAddress="172.20.125.182";IsPrivate=$True}
        @{IPAddress="172.28.48.161";IsPrivate=$True}
        @{IPAddress="172.16.189.59";IsPrivate=$True}
        @{IPAddress="172.29.158.136";IsPrivate=$True}
        @{IPAddress="172.20.130.104";IsPrivate=$True}
        @{IPAddress="172.22.156.161";IsPrivate=$True}
        @{IPAddress="172.27.121.31";IsPrivate=$True}
        @{IPAddress="172.20.125.91";IsPrivate=$True}
        @{IPAddress="172.16.154.81";IsPrivate=$True}
        @{IPAddress="172.30.218.232";IsPrivate=$True}
        @{IPAddress="192.168.89.53";IsPrivate=$True}
        @{IPAddress="192.168.145.82";IsPrivate=$True}
        @{IPAddress="192.168.218.187";IsPrivate=$True}
        @{IPAddress="192.168.199.193";IsPrivate=$True}
        @{IPAddress="192.168.38.180";IsPrivate=$True}
        @{IPAddress="192.168.113.230";IsPrivate=$True}
        @{IPAddress="192.168.151.221";IsPrivate=$True}
        @{IPAddress="192.168.7.201";IsPrivate=$True}
        @{IPAddress="192.168.0.112";IsPrivate=$True}
        @{IPAddress="192.168.80.16";IsPrivate=$True}
        @{IPAddress="15.58.220.0";IsPrivate=$False}
        @{IPAddress="152.59.219.177";IsPrivate=$False}
        @{IPAddress="198.124.38.52";IsPrivate=$False}
        @{IPAddress="189.230.54.191";IsPrivate=$False}
        @{IPAddress="110.15.71.13";IsPrivate=$False}
        @{IPAddress="52.142.158.10";IsPrivate=$False}
        @{IPAddress="139.86.125.225";IsPrivate=$False}
        @{IPAddress="73.195.184.179";IsPrivate=$False}
        @{IPAddress="88.39.58.147";IsPrivate=$False}
        @{IPAddress="32.109.175.224";IsPrivate=$False}
        @{IPAddress="244.147.152.0";IsPrivate=$False}
        @{IPAddress="161.10.12.87";IsPrivate=$False}
        @{IPAddress="26.143.5.60";IsPrivate=$False}
        @{IPAddress="192.242.223.174";IsPrivate=$False}
        @{IPAddress="174.138.20.229";IsPrivate=$False}
        @{IPAddress="113.129.35.113";IsPrivate=$False}
        @{IPAddress="174.195.158.35";IsPrivate=$False}
        @{IPAddress="119.210.82.193";IsPrivate=$False}
        @{IPAddress="118.191.220.103";IsPrivate=$False}
        @{IPAddress="105.220.90.101";IsPrivate=$False}
        @{IPAddress="28.38.55.86";IsPrivate=$False}
        @{IPAddress="118.237.6.175";IsPrivate=$False}
        @{IPAddress="187.15.22.183";IsPrivate=$False}
        @{IPAddress="193.111.13.238";IsPrivate=$False}
        @{IPAddress="252.166.166.86";IsPrivate=$False}
        @{IPAddress="69.84.176.157";IsPrivate=$False}
        @{IPAddress="30.44.46.94";IsPrivate=$False}
        @{IPAddress="102.94.43.175";IsPrivate=$False}
        @{IPAddress="125.246.80.89";IsPrivate=$False}
        @{IPAddress="65.204.65.165";IsPrivate=$False}
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $ExpectedParameterList = @(
            'IPAddress','Verbose',
            'Debug','ErrorAction','WarningAction','InformationAction',
            'ErrorVariable','WarningVariable','InformationVariable',
            'OutVariable','OutBuffer','PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            # it "has the correct default parameter set" {
            #     $Function.DefaultParameterSet | Should Be 'Mask'
            # }
        }

        Context "ParameterSet: Default" {

            it "executes with correct data (<ipaddress> : <IsPrivate>)" -TestCases $TestTable {
                param([ipaddress]$IPAddress,[int]$IsPrivate)
                {TestIPAddressIsPrivate -IPAddress $IPAddress} | Should Not Throw
                TestIPAddressIsPrivate -IPAddress $IPAddress | Should Be $IsPrivate
            }
        }
    }
}