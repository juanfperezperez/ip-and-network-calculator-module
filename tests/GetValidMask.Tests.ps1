Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'GetValidMask'
    $TestTable = @(
        @{IPAddress="0.0.0.0";OrderIndex = 0 }
        @{IPAddress="128.0.0.0";OrderIndex = 1 }
        @{IPAddress="192.0.0.0";OrderIndex = 2 }
        @{IPAddress="224.0.0.0";OrderIndex = 3 }
        @{IPAddress="240.0.0.0";OrderIndex = 4 }
        @{IPAddress="248.0.0.0";OrderIndex = 5 }
        @{IPAddress="252.0.0.0";OrderIndex = 6 }
        @{IPAddress="254.0.0.0";OrderIndex = 7 }
        @{IPAddress="255.0.0.0";OrderIndex = 8 }
        @{IPAddress="255.128.0.0";OrderIndex = 9 }
        @{IPAddress="255.192.0.0";OrderIndex = 10 }
        @{IPAddress="255.224.0.0";OrderIndex = 11 }
        @{IPAddress="255.240.0.0";OrderIndex = 12 }
        @{IPAddress="255.248.0.0";OrderIndex = 13 }
        @{IPAddress="255.252.0.0";OrderIndex = 14 }
        @{IPAddress="255.254.0.0";OrderIndex = 15 }
        @{IPAddress="255.255.0.0";OrderIndex = 16 }
        @{IPAddress="255.255.128.0";OrderIndex = 17 }
        @{IPAddress="255.255.192.0";OrderIndex = 18 }
        @{IPAddress="255.255.224.0";OrderIndex = 19 }
        @{IPAddress="255.255.240.0";OrderIndex = 20 }
        @{IPAddress="255.255.248.0";OrderIndex = 21 }
        @{IPAddress="255.255.252.0";OrderIndex = 22 }
        @{IPAddress="255.255.254.0";OrderIndex = 23 }
        @{IPAddress="255.255.255.0";OrderIndex = 24 }
        @{IPAddress="255.255.255.128";OrderIndex = 25 }
        @{IPAddress="255.255.255.192";OrderIndex = 26 }
        @{IPAddress="255.255.255.224";OrderIndex = 27 }
        @{IPAddress="255.255.255.240";OrderIndex = 28 }
        @{IPAddress="255.255.255.248";OrderIndex = 29 }
        @{IPAddress="255.255.255.252";OrderIndex = 30 }
        @{IPAddress="255.255.255.254";OrderIndex = 31 }
        @{IPAddress="255.255.255.255";OrderIndex = 32 }
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        # $ExpectedParameterList = @(
        #     'IPAddress','Verbose',
        #     'Debug','ErrorAction','WarningAction','InformationAction',
        #     'ErrorVariable','WarningVariable','InformationVariable',
        #     'OutVariable','OutBuffer','PipelineVariable'
        # ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            # it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
            #     param($Parameter)
            #     $Parameter | Should BeIn $Function.Parameters.Keys
            # }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            # it "has the correct default parameter set" {
            #     $Function.DefaultParameterSet | Should Be 'Mask'
            # }
        }

        Context "ParameterSet: Default" {

            it "executes with correct data (<OrderIndex> : <IPAddress>)" -TestCases $TestTable {
                param([ipaddress]$IPAddress,[int]$OrderIndex)
                {GetValidMask} | Should Not Throw
                $(GetValidMask)[$OrderIndex] | Should Be $IPAddress
                $(GetValidMask).IndexOf($IPAddress) | Should Be $OrderIndex
            }
        }
    }
}