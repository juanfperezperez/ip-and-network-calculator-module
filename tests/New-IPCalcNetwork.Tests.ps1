Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force

    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $DefaultParameterSet = 'NetworkMask'
        $ExpectedParameterList = @(
            'NetworkID','NetworkMask','NetworkMaskLength','Verbose','Debug',
            'ErrorAction','WarningAction','InformationAction','ErrorVariable',
            'WarningVariable','InformationVariable','OutVariable','OutBuffer',
            'PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            it "has a synopsis" {
                $helpObject.Synopsis | Should Not BeNullOrEmpty
            }
            it "has a description" {
                $helpObject.Description | Should Not BeNullOrEmpty
            }
            it "has at least 1 example" {
                $helpObject.Examples | Should Not BeNullOrEmpty
                $helpObject.Examples.Example | Should Not BeNullOrEmpty
                ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }
        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            it "has the correct default parameter set" {
                $Function.DefaultParameterSet | Should Be $DefaultParameterSet
            }
        }

        Context "ParameterSet: NetworkMask" {
            $ParameterSetName = 'NetworkMask'
            $ExpectedParameterSetParams = @(
                'NetworkID','NetworkMask','Verbose','Debug',
                'ErrorAction','WarningAction','InformationAction','ErrorVariable',
                'WarningVariable','InformationVariable','OutVariable','OutBuffer',
                'PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data" {
                {New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMask '255.255.255.0' } | Should Not Throw
                $NWObj = New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMask '255.255.255.0'
                $NWObj.ID | Should Be ([ipaddress]'192.168.1.0')
                $NWObj.Mask | Should Be ([ipaddress]'255.255.255.0')
                $NWObj.MaskLength | Should Be 24
            }

            it "throws with incorrect data" {
                {New-IPCalcNetwork -NetworkID '192.168.1' -NetworkMask '255.255.255.0'} | Should Throw
                {New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMask '255.255.255.22'} | Should Throw
            }

            $TestObjProps = @{prop='Id';value=([ipaddress]'192.168.1.0')},
                            @{prop='Mask';value=([ipaddress]'255.255.255.0')},
                            @{prop='MaskLength';value=24},
                            @{prop='MaxAddressCount';value=256},
                            @{prop='MaxHostCount';value=254},
                            @{prop='BroadcastAddress';value=[ipaddress]'192.168.1.255'},
                            @{prop='AddressClass';value='C'},
                            @{prop='IsPrivate';value=$true},
                            @{prop='MaxSubnetCount';value=64},
                            @{prop='FirstHost';value=[ipaddress]'192.168.1.1'},
                            @{prop='LastHost';value=[ipaddress]'192.168.1.254'}
            it "returns <value> for <prop> property" -TestCases $TestObjProps {
                param($prop,$value)
                {New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMask '255.255.255.0' } | Should Not Throw
                $NWObj = New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMask '255.255.255.0' -ErrorAction SilentlyContinue
                $NWObj | Should Not BeNullOrEmpty
                $NWObj.$prop | Should Be $value
            }

        }

        Context "ParameterSet: NetworkMaskLength" {
            $ParameterSetName = 'NetworkMaskLength'
            $ExpectedParameterSetParams = @(
                'NetworkID','NetworkMaskLength','Verbose','Debug',
                'ErrorAction','WarningAction','InformationAction','ErrorVariable',
                'WarningVariable','InformationVariable','OutVariable','OutBuffer',
                'PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data" {
                {New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMaskLength 24 } | Should Not Throw
                $NWObj = New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMaskLength 24
                $NWObj.Id | Should Be ([ipaddress]'192.168.1.0')
                $NWObj.Mask | Should Be ([ipaddress]'255.255.255.0')
                $NWObj.MaskLength | Should Be 24
            }

            it "throws with incorrect data" {
                {New-IPCalcNetwork -NetworkID '192.168.1' -NetworkMaskLength 24} | Should Throw
                {New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMaskLength 33} | Should Throw
            }

            $TestObjProps = @{prop='Id';value=([ipaddress]'192.168.1.0')},
                            @{prop='Mask';value=([ipaddress]'255.255.255.0')},
                            @{prop='MaskLength';value=24},
                            @{prop='MaxAddressCount';value=256},
                            @{prop='MaxHostCount';value=254},
                            @{prop='BroadcastAddress';value=[ipaddress]'192.168.1.255'},
                            @{prop='AddressClass';value='C'},
                            @{prop='IsPrivate';value=$true},
                            @{prop='MaxSubnetCount';value=64},
                            @{prop='FirstHost';value=[ipaddress]'192.168.1.1'},
                            @{prop='LastHost';value=[ipaddress]'192.168.1.254'}
            it "returns <value> for <prop> property" -TestCases $TestObjProps {
                param($prop,$value)
                {New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMaskLength 24 } | Should Not Throw
                $NWObj = New-IPCalcNetwork -NetworkID '192.168.1.0' -NetworkMaskLength 24 -ErrorAction SilentlyContinue
                $NWObj | Should Not BeNullOrEmpty
                $NWObj.$prop | Should Be $value
            }
            
        }

        Context "Return Object Basic Methods" {
            $TestObjForMethods = @(
                @{IP='192.168.1.0';MaskLength='25';FirstIP='192.168.1.1';LastIP='192.168.1.126';HostCount=126}
                @{IP='192.168.1.0';MaskLength='24';FirstIP='192.168.1.1';LastIP='192.168.1.254';HostCount=254}
                @{IP='192.168.0.0';MaskLength='23';FirstIP='192.168.0.1';LastIP='192.168.1.254';HostCount=510}
            )
            # it "returns the correct number of host addresses for <IP>/<MaskLength>" -TestCases $TestObjForMethods {
            #     param($IP,$MaskLength,$FirstIP,$LastIP,$HostCount)
            #     {New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength} | Should Not Throw
            #     (New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).GetHostAddresses().Count | Should Be $HostCount
            # }
            # it "returns the correct first address for <IP>/<MaskLength>" -TestCases $TestObjForMethods {
            #     param($IP,$MaskLength,$FirstIP,$LastIP,$HostCount)
            #     {New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength} | Should Not Throw
            #     (New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).GetFirstHostAddress() | Should Be ([ipaddress]$FirstIP)
            # }
            # it "returns the correct last address for <IP>/<MaskLength>" -TestCases $TestObjForMethods {
            #     param($IP,$MaskLength,$FirstIP,$LastIP,$HostCount)
            #     {New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength} | Should Not Throw
            #     (New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).GetLastHostAddress() | Should Be ([ipaddress]$LastIP)
            # }
            it "returns the correct string representation for <IP>/<MaskLength>" -TestCases $TestObjForMethods {
                param($IP,$MaskLength,$FirstIP,$LastIP,$HostCount)
                {New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength} | Should Not Throw
                (New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).ToString() | Should Be "$IP/$MaskLength"
            }
        }

        Context "Return Object Advanced Methods" {
            $AdvancedMethodsTestCases = @(
                @{IP='192.168.1.0';MaskLength='25';split=4;GoodSplits=@(2,4,8,16);BadSplits=@(256,512,1024);TestSplits=@('192.168.1.0','192.168.1.32','192.168.1.64','192.168.1.96')}
                @{IP='192.168.1.0';MaskLength='24';split=4;GoodSplits=@(2,4,8,16);BadSplits=@(512,1024);TestSplits=@('192.168.1.0','192.168.1.64','192.168.1.128','192.168.1.192')}
                @{IP='192.168.0.0';MaskLength='23';split=4;GoodSplits=@(2,4,8,16);BadSplits=@(1024);TestSplits=@('192.168.0.0','192.168.0.128','192.168.1.0','192.168.1.128')}
            )
            
            it "Split: Accepts the correct numbers for mask length <MaskLength>" -TestCases $AdvancedMethodsTestCases {
                param($IP,$MaskLength,$GoodSplits,$BadSplits,$split,$TestSplits)
                {New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength} | Should Not Throw
                foreach($split in $GoodSplits){
                    {(New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).Split($split)} | Should Not Throw
                }
                foreach($split in $BadSplits){
                    {(New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).Split($split)} | Should Throw
                }

            }
            it "Split: Returns the correct networks for <IP>/<MaskLength> split into <split>" -TestCases $AdvancedMethodsTestCases {
                param($IP,$MaskLength,$GoodSplits,$BadSplits,$split,$TestSplits)
                {New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength} | Should Not Throw
                foreach($Gsplit in $GoodSplits){
                    
                    {(New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).Split($Gsplit)} | Should Not Throw
                    (New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).Split($Gsplit).Count | Should Be $Gsplit
                    foreach($GoodSplit in $TestSplits){
                       $TestSplits | Should -Be (New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).Split($split).Id.IPAddressToString  
                    }
                    foreach($NW in (New-IPCalcNetwork -NetworkID $IP -NetworkMaskLength $MaskLength).Split($split)){
                        $NW.Id.IPAddressToString | Should -BeIn $TestSplits
                    }
                    
                }
            }

        }
    }