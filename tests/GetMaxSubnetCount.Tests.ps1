Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'GetMaxSubnetCount'
    $TestTable = @(
        @{MaskLength = 0; Mask = "0.0.0.0"; Total = 1073741824}
        @{MaskLength = 1; Mask = "128.0.0.0"; Total = 536870912}
        @{MaskLength = 2; Mask = "192.0.0.0"; Total = 268435456}
        @{MaskLength = 3; Mask = "224.0.0.0"; Total = 134217728}
        @{MaskLength = 4; Mask = "240.0.0.0"; Total = 67108864}
        @{MaskLength = 5; Mask = "248.0.0.0"; Total = 33554432}
        @{MaskLength = 6; Mask = "252.0.0.0"; Total = 16777216}
        @{MaskLength = 7; Mask = "254.0.0.0"; Total = 8388608}
        @{MaskLength = 8; Mask = "255.0.0.0"; Total = 4194304}
        @{MaskLength = 9; Mask = "255.128.0.0"; Total = 2097152}
        @{MaskLength = 10; Mask = "255.192.0.0"; Total = 1048576}
        @{MaskLength = 11; Mask = "255.224.0.0"; Total = 524288}
        @{MaskLength = 12; Mask = "255.240.0.0"; Total = 262144}
        @{MaskLength = 13; Mask = "255.248.0.0"; Total = 131072}
        @{MaskLength = 14; Mask = "255.252.0.0"; Total = 65536}
        @{MaskLength = 15; Mask = "255.254.0.0"; Total = 32768}
        @{MaskLength = 16; Mask = "255.255.0.0"; Total = 16384}
        @{MaskLength = 17; Mask = "255.255.128.0"; Total = 8192}
        @{MaskLength = 18; Mask = "255.255.192.0"; Total = 4096}
        @{MaskLength = 19; Mask = "255.255.224.0"; Total = 2048}
        @{MaskLength = 20; Mask = "255.255.240.0"; Total = 1024}
        @{MaskLength = 21; Mask = "255.255.248.0"; Total = 512}
        @{MaskLength = 22; Mask = "255.255.252.0"; Total = 256}
        @{MaskLength = 23; Mask = "255.255.254.0"; Total = 128}
        @{MaskLength = 24; Mask = "255.255.255.0"; Total = 64}
        @{MaskLength = 25; Mask = "255.255.255.128"; Total = 32}
        @{MaskLength = 26; Mask = "255.255.255.192"; Total = 16}
        @{MaskLength = 27; Mask = "255.255.255.224"; Total = 8}
        @{MaskLength = 28; Mask = "255.255.255.240"; Total = 4}
        @{MaskLength = 29; Mask = "255.255.255.248"; Total = 2}
        @{MaskLength = 30; Mask = "255.255.255.252"; Total = 0}
        @{MaskLength = 31; Mask = "255.255.255.254"; Total = 0}
        @{MaskLength = 32; Mask = "255.255.255.255"; Total = 0}
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $ExpectedParameterList = @(
            'Mask','MaskLength','Verbose',
            'Debug','ErrorAction','WarningAction','InformationAction',
            'ErrorVariable','WarningVariable','InformationVariable',
            'OutVariable','OutBuffer','PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            it "has the correct default parameter set" {
                $Function.DefaultParameterSet | Should Be 'Mask'
            }
        }

        Context "ParameterSet: Mask" {
            $ParameterSetName = 'Mask'
            $ExpectedParameterSetParams = @(
                'Mask','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<Mask>: <Total>)" -TestCases $TestTable {
                param([ipaddress]$Mask,[int]$MaskLength,[int64]$Total,[int64]$Usable)
                {GetMaxSubnetCount -Mask $Mask} | Should Not Throw
                GetMaxSubnetCount -Mask $Mask | Should Be $Total
            }
        }
        Context "ParameterSet: MaskLength" {
            $ParameterSetName = 'MaskLength'
            $ExpectedParameterSetParams = @(
                'MaskLength','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<MaskLength>: <Total>)" -TestCases $TestTable {
                param([ipaddress]$Mask,[int]$MaskLength,[int64]$Total,[int64]$Usable)
                {GetMaxSubnetCount -MaskLength $MaskLength} | Should Not Throw
                GetMaxSubnetCount -MaskLength $MaskLength | Should Be $Total
            }
        }
    }
}