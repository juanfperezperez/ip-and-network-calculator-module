Param(
    [string]$ModuleName=$null
)

$TestsDirectory =      Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory =      Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$PublicFunctionsPath =  Join-Path -Path $SourceDirectory -ChildPath Public
$PrivateFunctionsPath = Join-Path -Path $SourceDirectory -ChildPath Private

$Languages = @(
    'af','af-ZA','ar','ar-AE','ar-BH','ar-DZ','ar-EG','ar-IQ','ar-JO','ar-KW','ar-LB','ar-LY','ar-MA','ar-OM','ar-QA',
    'ar-SA','ar-SY','ar-TN','ar-YE','az','az-Cyrl-AZ','az-Latn-AZ','be','be-BY','bg','bg-BG','ca','ca-ES','cs','cs-CZ',
    'da','da-DK','de','de-AT','de-CH','de-DE','de-LI','de-LU','dv','dv-MV','el','el-GR','en','en-029','en-AU','en-BZ',
    'en-CA','en-GB','en-IE','en-JM','en-NZ','en-PH','en-TT','en-US','en-ZA','en-ZW','es','es-AR','es-BO','es-CL',
    'es-CO','es-CR','es-DO','es-EC','es-ES','es-GT','es-HN','es-MX','es-NI','es-PA','es-PE','es-PR','es-PY','es-SV',
    'es-UY','es-VE','et','et-EE','eu','eu-ES','fa','fa-IR','fi','fi-FI','fo','fo-FO','fr','fr-BE','fr-CA','fr-CH',
    'fr-FR','fr-LU','fr-MC','gl','gl-ES','gu','gu-IN','he','he-IL','hi','hi-IN','hr','hr-HR','hu','hu-HU','hy','hy-AM',
    'id','id-ID','is','is-IS','it','it-CH','it-IT','ja','ja-JP','ka','ka-GE','kk','kk-KZ','kn','kn-IN','ko','ko-KR',
    'kok','kok-IN','ky','ky-KG','lt','lt-LT','lv','lv-LV','mk','mk-MK','mn','mn-MN','mr','mr-IN','ms','ms-BN','ms-MY',
    'nb-NO','nl','nl-BE','nl-NL','nn-NO','no','pa','pa-IN','pl','pl-PL','pt','pt-BR','pt-PT','ro','ro-RO','ru','ru-RU',
    'sa','sa-IN','sk','sk-SK','sl','sl-SI','sq','sq-AL','sr','sv','sv-FI','sv-SE','sw','sw-KE','syr','syr-SY','ta',
    'ta-IN','te','te-IN','th','th-TH','tr','tr-TR','tt','tt-RU','uk','uk-UA','ur','ur-PK','uz','uz-Cyrl-UZ',
    'uz-Latn-UZ','vi','vi-VN','zh-CN','zh-Hans','zh-Hant','zh-HK','zh-MO','zh-SG','zh-TW','zh-CHS','zh-CHT'
)

#Functions
function Test-ValidPowerShell {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,ParameterSetName='Path')]
        [ValidateScript({Test-Path -Path $_})]
        [string]$Path,
        [Parameter(Mandatory=$true,ParameterSetName='ScriptBlock')]
        [scriptblock]$ScriptBlock
    )

    begin {
    }
    
    process {
        try {
            $Errors = [System.Management.Automation.Language.ParseError[]]@()
            $Tokens  = [System.Management.Automation.Language.Token[]]@()
            switch ($PSCmdlet.ParameterSetName) {
                Path {
                    $FullPath = Resolve-Path -Path $Path
                    $Result = [Management.Automation.Language.Parser]::ParseFile($FullPath,[ref]$Tokens,[ref]$Errors)
                }
                ScriptBlock {
                    $Result = [Management.Automation.Language.Parser]::ParseFile($ScriptBlock.ToString(),$null,[ref]$Tokens,[ref]$Errors)
                }
            }
            New-Object -TypeName psobject -Property @{
                AST = $Result
                Tokens = $Tokens
                Errors = $Errors
            } | Add-Member -MemberType ScriptProperty -Name ValidPowerShell -Value {($this.Errors.Count -eq 0)} -PassThru
        }
        catch {
            $PSCmdlet.ThrowTerminatingError($_)
        }
    }
    
    end {
    }
}

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

Describe "Module: $ModuleName" -Tag Structure {
    Describe "Main Module Components" {
        # PSD1
        Context "Manifest" {
            $ManifestFilePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"
            
            It "contains a manifest file" {
                $ManifestFilePath | Should Exist
            }

            It "contains a valid manifest file" {
                $ManifestFilePath | Should Exist
                (Test-ValidPowerShell -Path $ManifestFilePath).ValidPowerShell | Should Be $true
                {Test-ModuleManifest -Path $ManifestFilePath} | Should Not Throw
            }
            
            #<#
            $RequiredManifestProps = @(
                'Author','Description'
            ) | ForEach-Object {@{'PropName'=$_}}
            It "contains <PropName> key" -TestCases $RequiredManifestProps {
                param($PropName)
                {Test-ModuleManifest -Path $ManifestFilePath} | Should Not Throw
                (Invoke-Expression (Get-Content -Raw -Path $ManifestFilePath))[$PropName] | Should Not BeNullOrEmpty
            }
            #>

        }

        # PSM1
        Context "Main Module File" {
            $MainModuleFilePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psm1"
            It "contains main module file" {
                $MainModuleFilePath | Should Exist
            }
            It "is valid PowerShell" {
                $MainModuleFilePath | Should Exist
                (Test-ValidPowerShell -Path $MainModuleFilePath -ErrorAction SilentlyContinue).ValidPowerShell | Should Be $true
            }
        }
    }

    # Public
    Describe "Public Functions" {
        it "contains a 'Public' folder with function files" {
            $PublicFunctionsPath | Should Exist
            Get-ChildItem -Path $PublicFunctionsPath -Filter '*-*.ps1' | Should Not BeNullOrEmpty
        }
        foreach($file in (Get-ChildItem -Path $PublicFunctionsPath -Filter '*-*.ps1' -ErrorAction SilentlyContinue)){
            Context "Function: $($File.BaseName)" {

                $ValidPowershell = Test-ValidPowerShell -Path $File.FullName
                
                It "is valid powershell" {
                    $ValidPowershell.ValidPowerShell | Should Be $true
                }
                
                It "passes PSScriptAnalyzer analisys" {
                    Invoke-ScriptAnalyzer -Path $file.FullName
                }
                It "contains one function per file" {
                    $ValidPowershell.ValidPowerShell             | Should Be $true
                    $ValidPowershell.AST.Attributes.Count        | Should Be 0
                    $ValidPowershell.AST.UsingStatements.Count   | Should Be 0
                    $ValidPowershell.AST.ParamBlock              | Should BeNullOrEmpty
                    $ValidPowershell.AST.BeginBlock              | Should BeNullOrEmpty
                    $ValidPowershell.AST.ProcessBlock            | Should BeNullOrEmpty
                    $ValidPowershell.AST.DynamicParamBlock       | Should BeNullOrEmpty
                    $ValidPowershell.AST.ScriptRequirements      | Should BeNullOrEmpty
                    $ValidPowershell.AST.Parent                  | Should BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Unnamed        | Should Be $true
                    $ValidPowershell.AST.EndBlock.Traps          | Should BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Statements.Count   | Should Be 1
                    $ValidPowershell.AST.EndBlock.Statements[0].Name | Should Be ([System.IO.Path]::GetFileNameWithoutExtension($ValidPowershell.AST.EndBlock.Statements[0].Extent.File))
                    $ValidPowershell.AST.EndBlock.Statements[0].GetType().FullName | SHould Be 'System.Management.Automation.Language.FunctionDefinitionAst'
                }
                
                It "contains help" {
                    $ValidPowershell.ValidPowerShell | Should Be $true
                    $ValidPowershell.AST.EndBlock.Statements[0].GetHelpContent()             | Should Not BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Statements[0].GetHelpContent().Synopsis    | Should Not BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Statements[0].GetHelpContent().Description | Should Not BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Statements[0].GetHelpContent().Notes       | Should Not BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Statements[0].GetHelpContent().Inputs      | Should Not BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Statements[0].GetHelpContent().Outputs     | Should Not BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Statements[0].GetHelpContent().Examples    | Should Not BeNullOrEmpty
                }
                
                it "is an advanced function" {
                    $ValidPowershell.ValidPowerShell | Should Be $true
                    $ValidPowershell.AST.EndBlock.Statements[0].Body.ParamBlock | Should Not BeNullOrEmpty
                    'CmdletBinding' | Should BeIn $ValidPowershell.AST.EndBlock.Statements[0].Body.ParamBlock.Attributes.TypeName.Name
                }
                <#
                #>
            }
        }
    }
    
    # Private
    Describe "Private Functions" {
        it "contains a 'Private' folder with function files" {
            $PrivateFunctionsPath | Should Exist
            Get-ChildItem -Path $PrivateFunctionsPath -Filter '*.ps1' | Should Not BeNullOrEmpty
        }
        foreach($file in (Get-ChildItem -Path $PrivateFunctionsPath -Filter '*.ps1' -ErrorAction SilentlyContinue)){
            Context "Function: $($File.BaseName)" {

                $ValidPowershell = Test-ValidPowerShell -Path $File.FullName
                
                It "is valid powershell" {
                    $ValidPowershell.ValidPowerShell | Should Be $true
                }
                
                It "passes PSScriptAnalyzer analisys" {
                    Invoke-ScriptAnalyzer -Path $file.FullName
                }
                
                It "has a corresponding test" {
                    $testFileName = $File.Name -replace '\.ps1$','.Tests.ps1'
                    Join-Path -Path $TestsDirectory -ChildPath $testFileName | Should Exist
                }

                It "contains one function per file" {
                    $ValidPowershell.ValidPowerShell             | Should Be $true
                    $ValidPowershell.AST.Attributes.Count        | Should Be 0
                    $ValidPowershell.AST.UsingStatements.Count   | Should Be 0
                    $ValidPowershell.AST.ParamBlock              | Should BeNullOrEmpty
                    $ValidPowershell.AST.BeginBlock              | Should BeNullOrEmpty
                    $ValidPowershell.AST.ProcessBlock            | Should BeNullOrEmpty
                    $ValidPowershell.AST.DynamicParamBlock       | Should BeNullOrEmpty
                    $ValidPowershell.AST.ScriptRequirements      | Should BeNullOrEmpty
                    $ValidPowershell.AST.Parent                  | Should BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Unnamed        | Should Be $true
                    $ValidPowershell.AST.EndBlock.Traps          | Should BeNullOrEmpty
                    $ValidPowershell.AST.EndBlock.Statements.Count   | Should Be 1
                    $ValidPowershell.AST.EndBlock.Statements[0].Name | Should Be ([System.IO.Path]::GetFileNameWithoutExtension($ValidPowershell.AST.EndBlock.Statements[0].Extent.File))
                    $ValidPowershell.AST.EndBlock.Statements[0].GetType().FullName | SHould Be 'System.Management.Automation.Language.FunctionDefinitionAst'
                }
                
                <#
                It "contains help" {
                    $ValidPowershell.ValidPowerShell | Should Be $true
                    # AST code for making sure there is help
                    # $ValidPowershell.Ast
                }
                
                it "is an advanced function" {
                    $ValidPowershell.ValidPowerShell | Should Be $true
                    # AST code for making sure it is an advanced function
                    # $ValidPowershell.Ast
                }
                #>
            }
        }
    }
    
    # Module Help
    Describe "Module Help"{
        $HelpFolders = Get-ChildItem -Path $SourceDirectory -Directory | Where-Object {$_.BaseName -in $Languages}
        
        it "Contains a help folder" {
            $HelpFolders | Should Not BeNullOrEmpty
        }

        foreach($folder in $HelpFolders){
            Context "Language specific help ($($folder.BaseName))"{
                it "contains about_$ModuleName file for language" {
                    Join-Path -Path $folder.FullName -ChildPath "about_$ModuleName.help.txt" | Should Exist
                }
            }
        }

    }
}

