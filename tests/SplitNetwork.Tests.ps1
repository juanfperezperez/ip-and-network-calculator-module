Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'SplitNetwork'
    $TestTable = @(
        @{IPAddress='192.168.1.0';Mask='255.255.255.128';MaskLength=25;MinSubnetCount=2;ActualSubnetCount='2';NewMaskLength=26;Children=@('192.168.1.0','192.168.1.64')}
        @{IPAddress='192.168.1.0';Mask='255.255.255.128';MaskLength=25;MinSubnetCount=3;ActualSubnetCount='4';NewMaskLength=27;Children=@('192.168.1.0','192.168.1.32','192.168.1.64','192.168.1.96')}
        @{IPAddress='192.168.1.0';Mask='255.255.255.128';MaskLength=25;MinSubnetCount=5;ActualSubnetCount='8';NewMaskLength=28;Children=@('192.168.1.0','192.168.1.16','192.168.1.32','192.168.1.48','192.168.1.64','192.168.1.80','192.168.1.96','192.168.1.112')}
        @{IPAddress='192.168.1.0';Mask='255.255.255.128';MaskLength=25;MinSubnetCount=10;ActualSubnetCount='16';NewMaskLength=29;Children=@('192.168.1.0','192.168.1.8','192.168.1.16','192.168.1.24','192.168.1.32','192.168.1.40','192.168.1.48','192.168.1.56','192.168.1.64','192.168.1.72','192.168.1.80','192.168.1.88','192.168.1.96','192.168.1.104','192.168.1.112','192.168.1.120')}
        @{IPAddress='192.168.1.0';Mask='255.255.255.0';MaskLength=24;MinSubnetCount=2;ActualSubnetCount='2';NewMaskLength=25;Children=@('192.168.1.0','192.168.1.128')}
        @{IPAddress='192.168.1.0';Mask='255.255.255.0';MaskLength=24;MinSubnetCount=3;ActualSubnetCount='4';NewMaskLength=26;Children=@('192.168.1.0','192.168.1.64','192.168.1.128','192.168.1.192')}
        @{IPAddress='192.168.1.0';Mask='255.255.255.0';MaskLength=24;MinSubnetCount=5;ActualSubnetCount='8';NewMaskLength=27;Children=@('192.168.1.0','192.168.1.32','192.168.1.64','192.168.1.96','192.168.1.128','192.168.1.160','192.168.1.192','192.168.1.224')}
        @{IPAddress='192.168.1.0';Mask='255.255.255.0';MaskLength=24;MinSubnetCount=10;ActualSubnetCount='16';NewMaskLength=28;Children=@('192.168.1.0','192.168.1.16','192.168.1.32','192.168.1.48','192.168.1.64','192.168.1.80','192.168.1.96','192.168.1.112','192.168.1.128','192.168.1.144','192.168.1.160','192.168.1.176','192.168.1.192','192.168.1.208','192.168.1.224','192.168.1.240')}
        @{IPAddress='192.168.0.0';Mask='255.255.254.0';MaskLength=23;MinSubnetCount=2;ActualSubnetCount='2';NewMaskLength=24;Children=@('192.168.0.0','192.168.1.0')}
        @{IPAddress='192.168.0.0';Mask='255.255.254.0';MaskLength=23;MinSubnetCount=3;ActualSubnetCount='4';NewMaskLength=25;Children=@('192.168.0.0','192.168.0.128','192.168.1.0','192.168.1.128')}
        @{IPAddress='192.168.0.0';Mask='255.255.254.0';MaskLength=23;MinSubnetCount=5;ActualSubnetCount='8';NewMaskLength=26;Children=@('192.168.0.0','192.168.0.64','192.168.0.128','192.168.0.192','192.168.1.0','192.168.1.64','192.168.1.128','192.168.1.192')}
        @{IPAddress='192.168.0.0';Mask='255.255.254.0';MaskLength=23;MinSubnetCount=10;ActualSubnetCount='16';NewMaskLength=27;Children=@('192.168.0.0','192.168.0.32','192.168.0.64','192.168.0.96','192.168.0.128','192.168.0.160','192.168.0.192','192.168.0.224','192.168.1.0','192.168.1.32','192.168.1.64','192.168.1.96','192.168.1.128','192.168.1.160','192.168.1.192','192.168.1.224')}
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $ExpectedParameterList = @(
            'IPAddress','Mask','MaskLength','Verbose','SubnetCount',
            'Debug','ErrorAction','WarningAction','InformationAction',
            'ErrorVariable','WarningVariable','InformationVariable',
            'OutVariable','OutBuffer','PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            it "has the correct default parameter set" {
                $Function.DefaultParameterSet | Should Be 'Mask'
            }
        }

        Context "ParameterSet: Mask" {
            $ParameterSetName = 'Mask'
            $ExpectedParameterSetParams = @(
                'IPAddress','Mask','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable','SubnetCount',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<IPAddress>/<Mask>: <MinSubnetCount>,<ActualSubnetCount>)" -TestCases $TestTable {
                param([ipaddress]$IPAddress,[ipaddress]$Mask,[int]$MaskLength,[int]$MinSubnetCount,[int]$ActualSubnetCount,[int]$NewMaskLength,[ipaddress[]]$Children)
                {SplitNetwork -IPAddress $IPAddress -Mask $Mask -SubnetCount $MinSubnetCount} | Should Not Throw
                $result = SplitNetwork -IPAddress $IPAddress -Mask $Mask -SubnetCount $MinSubnetCount
                $result.Count | Should Be $ActualSubnetCount
                for($i = 0; $i -lt $ActualSubnetCount; $i++){
                    $result[$i].IPAddress | Should Be $Children[$i]
                    $result[$i].MaskLength | Should Be $NewMaskLength
                }
            }
        }
        Context "ParameterSet: MaskLength" {
            $ParameterSetName = 'MaskLength'
            $ExpectedParameterSetParams = @(
                'IPAddress','MaskLength','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable','SubnetCount',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<IPAddress>/<MaskLength>: <MinSubnetCount>,<ActualSubnetCount>)" -TestCases $TestTable {
                param([ipaddress]$IPAddress,[ipaddress]$Mask,[int]$MaskLength,[int]$MinSubnetCount,[int]$ActualSubnetCount,[int]$NewMaskLength,[ipaddress[]]$Children)
                {SplitNetwork -IPAddress $IPAddress -MaskLength $MaskLength -SubnetCount $MinSubnetCount} | Should Not Throw
                $result = SplitNetwork -IPAddress $IPAddress -MaskLength $MaskLength -SubnetCount $MinSubnetCount
                $result.Count | Should Be $ActualSubnetCount
                for($i = 0; $i -lt $ActualSubnetCount; $i++){
                    $result[$i].IPAddress | Should Be $Children[$i]
                    $result[$i].MaskLength | Should Be $NewMaskLength
                }
            }
        }
    }
}