Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'GetIPAddressClass'
    $TestTable = @(
        @{IPAddress = '0.1.2.3'; Class = $null; Valid = $false}
        @{IPAddress = '127.0.0.1'; Class = 'LoopBackAndDiagnostics'; Valid = $true}
        @{IPAddress = '127.0.5.1'; Class = 'LoopBackAndDiagnostics'; Valid = $true}
        @{IPAddress = '10.0.0.0'; Class = 'A'; Valid = $true}
        @{IPAddress = '11.0.0.0'; Class = 'A'; Valid = $true}
        @{IPAddress = '128.0.0.0'; Class = 'B'; Valid = $true}
        @{IPAddress = '191.255.255.254'; Class = 'B'; Valid = $true}
        @{IPAddress = '172.16.2.3'; Class = 'B'; Valid = $true}
        @{IPAddress = '192.0.0.0'; Class = 'C'; Valid = $true}
        @{IPAddress = '192.168.0.0'; Class = 'C'; Valid = $true}
        @{IPAddress = '223.255.255.254'; Class = 'C'; Valid = $true}
        @{IPAddress = '224.0.0.0'; Class = 'D'; Valid = $true}
        @{IPAddress = '239.255.255.254'; Class = 'D'; Valid = $true}
        @{IPAddress = '240.0.0.0'; Class = 'E'; Valid = $true}
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $ExpectedParameterList = @(
            'IPAddress','List','Verbose',
            'Debug','ErrorAction','WarningAction','InformationAction',
            'ErrorVariable','WarningVariable','InformationVariable',
            'OutVariable','OutBuffer','PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
        }

        Context "ParameterSet: Calculate" {
            $ParameterSetName = 'Calculate'
            $ExpectedParameterSetParams = @(
                'IPAddress','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<IPAddress>: <Class>)" -TestCases $TestTable {
                param([ipaddress]$IPAddress,[string]$Class,[bool]$Valid)
                if($Valid){
                    {GetIPAddressClass -IPAddress $IPAddress} | Should Not Throw
                    GetIPAddressClass -IPAddress $IPAddress | Should Be $Class
                } else {
                    {GetIPAddressClass -IPAddress $IPAddress} | Should Throw
                }
            }
        }
        Context "ParameterSet: List" {
            $ParameterSetName = 'List'
            $ExpectedParameterSetParams = @(
                'List','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            $classTable = @(
                @{IPAddress = '0.0.0.0';Mask = '255.0.0.0';DefaultSubnetMask = $null;Class = 'NotValid'}
                @{IPAddress = '127.0.0.0';Mask = '255.0.0.0';DefaultSubnetMask = '255.0.0.0';Class = 'LoopBackAndDiagnostics'}
                @{IPAddress = '0.0.0.0';Mask = '128.0.0.0';DefaultSubnetMask = '255.0.0.0';Class = 'A'}
                @{IPAddress = '128.0.0.0';Mask = '192.0.0.0';DefaultSubnetMask = '255.255.0.0';Class = 'B'}
                @{IPAddress = '192.0.0.0';Mask = '224.0.0.0';DefaultSubnetMask = '255.255.255.0';Class = 'C'}
                @{IPAddress = '224.0.0.0';Mask = '240.0.0.0';DefaultSubnetMask = $null;Class = 'D'}
                @{IPAddress = '240.0.0.0';Mask = '240.0.0.0';DefaultSubnetMask = $null;Class = 'E'}
            )
            it "executes with correct data" {
                {GetIPAddressClass -List} | Should Not Throw
                $result = GetIPAddressClass -List
                $result.Count | Should Be 7
                foreach($i in $(0..6)){
                    $result[$i].IPAddress | Should Be $classTable[$i].IPAddress
                    $result[$i].Mask | Should Be $classTable[$i].Mask
                    $result[$i].DefaultSubnetMask | Should Be $classTable[$i].DefaultSubnetMask
                    $result[$i].Class | Should Be $classTable[$i].Class
                }
            }
        }
    }
}