Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'GetReverseIPAddress'
    $TestTable = @(
        @{IPAddress = "167.196.14.101"; Reverse = "101.14.196.167"}
        @{IPAddress = "212.21.136.108"; Reverse = "108.136.21.212"}
        @{IPAddress = "239.220.130.196"; Reverse = "196.130.220.239"}
        @{IPAddress = "101.237.197.65"; Reverse = "65.197.237.101"}
        @{IPAddress = "135.6.103.53"; Reverse = "53.103.6.135"}
        @{IPAddress = "158.45.5.143"; Reverse = "143.5.45.158"}
        @{IPAddress = "206.31.82.78"; Reverse = "78.82.31.206"}
        @{IPAddress = "189.104.187.101"; Reverse = "101.187.104.189"}
        @{IPAddress = "130.175.163.141"; Reverse = "141.163.175.130"}
        @{IPAddress = "115.54.69.245"; Reverse = "245.69.54.115"}
        @{IPAddress = "148.78.192.137"; Reverse = "137.192.78.148"}
        @{IPAddress = "52.254.191.204"; Reverse = "204.191.254.52"}
        @{IPAddress = "27.188.212.110"; Reverse = "110.212.188.27"}
        @{IPAddress = "98.195.191.183"; Reverse = "183.191.195.98"}
        @{IPAddress = "137.74.26.5"; Reverse = "5.26.74.137"}
        @{IPAddress = "103.72.160.157"; Reverse = "157.160.72.103"}
        @{IPAddress = "80.218.188.187"; Reverse = "187.188.218.80"}
        @{IPAddress = "14.244.187.0"; Reverse = "0.187.244.14"}
        @{IPAddress = "6.126.151.28"; Reverse = "28.151.126.6"}
        @{IPAddress = "7.223.91.244"; Reverse = "244.91.223.7"}
        @{IPAddress = "11.254.71.116"; Reverse = "116.71.254.11"}
        @{IPAddress = "118.195.113.155"; Reverse = "155.113.195.118"}
        @{IPAddress = "183.27.35.23"; Reverse = "23.35.27.183"}
        @{IPAddress = "72.47.172.113"; Reverse = "113.172.47.72"}
        @{IPAddress = "255.3.175.218"; Reverse = "218.175.3.255"}
        @{IPAddress = "30.158.27.78"; Reverse = "78.27.158.30"}
        @{IPAddress = "176.197.178.135"; Reverse = "135.178.197.176"}
        @{IPAddress = "233.94.67.76"; Reverse = "76.67.94.233"}
        @{IPAddress = "106.222.212.87"; Reverse = "87.212.222.106"}
        @{IPAddress = "201.156.93.12"; Reverse = "12.93.156.201"}
        @{IPAddress = "120.232.179.163"; Reverse = "163.179.232.120"}
        @{IPAddress = "10.143.214.242"; Reverse = "242.214.143.10"}
        @{IPAddress = "68.160.193.93"; Reverse = "93.193.160.68"}
        @{IPAddress = "178.252.17.21"; Reverse = "21.17.252.178"}
        @{IPAddress = "104.240.3.27"; Reverse = "27.3.240.104"}
        @{IPAddress = "231.253.16.196"; Reverse = "196.16.253.231"}
        @{IPAddress = "86.117.120.19"; Reverse = "19.120.117.86"}
        @{IPAddress = "90.188.108.13"; Reverse = "13.108.188.90"}
        @{IPAddress = "94.149.36.5"; Reverse = "5.36.149.94"}
        @{IPAddress = "197.223.249.30"; Reverse = "30.249.223.197"}
        @{IPAddress = "147.3.236.37"; Reverse = "37.236.3.147"}
        @{IPAddress = "228.217.52.46"; Reverse = "46.52.217.228"}
        @{IPAddress = "176.130.57.3"; Reverse = "3.57.130.176"}
        @{IPAddress = "216.162.2.120"; Reverse = "120.2.162.216"}
        @{IPAddress = "77.183.203.237"; Reverse = "237.203.183.77"}
        @{IPAddress = "4.162.111.4"; Reverse = "4.111.162.4"}
        @{IPAddress = "216.247.86.198"; Reverse = "198.86.247.216"}
        @{IPAddress = "45.171.164.96"; Reverse = "96.164.171.45"}
        @{IPAddress = "58.97.191.56"; Reverse = "56.191.97.58"}
        @{IPAddress = "162.184.154.91"; Reverse = "91.154.184.162"}
        @{IPAddress = "254.185.177.94"; Reverse = "94.177.185.254"}
        @{IPAddress = "180.38.59.69"; Reverse = "69.59.38.180"}
        @{IPAddress = "23.124.144.138"; Reverse = "138.144.124.23"}
        @{IPAddress = "16.184.197.142"; Reverse = "142.197.184.16"}
        @{IPAddress = "206.210.105.56"; Reverse = "56.105.210.206"}
        @{IPAddress = "129.157.195.13"; Reverse = "13.195.157.129"}
        @{IPAddress = "83.96.35.245"; Reverse = "245.35.96.83"}
        @{IPAddress = "82.38.211.5"; Reverse = "5.211.38.82"}
        @{IPAddress = "249.152.184.132"; Reverse = "132.184.152.249"}
        @{IPAddress = "238.123.150.70"; Reverse = "70.150.123.238"}
        @{IPAddress = "169.110.75.233"; Reverse = "233.75.110.169"}
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $ExpectedParameterList = @(
            'IPAddress','Verbose',
            'Debug','ErrorAction','WarningAction','InformationAction',
            'ErrorVariable','WarningVariable','InformationVariable',
            'OutVariable','OutBuffer','PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            # it "has the correct default parameter set" {
            #     $Function.DefaultParameterSet | Should Be 'Mask'
            # }
        }

        Context "ParameterSet: Default" {

            it "executes with correct data (<IPAddress> : <Reverse>)" -TestCases $TestTable {
                param([ipaddress]$IPAddress,[ipaddress]$Reverse)
                {GetReverseIPAddress -IPAddress $IPAddress} | Should Not Throw
                GetReverseIPAddress -IPAddress $IPAddress | Should Be $Reverse
            }
        }
    }
}