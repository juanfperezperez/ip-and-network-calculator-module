Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'GetMaxAddressCount'
    $TestTable = @(
        @{MaskLength = 0; Mask = "0.0.0.0"; Total = 4294967296; Usable = 4294967294}
        @{MaskLength = 1; Mask = "128.0.0.0"; Total = 2147483648; Usable = 2147483646}
        @{MaskLength = 2; Mask = "192.0.0.0"; Total = 1073741824; Usable = 1073741822}
        @{MaskLength = 3; Mask = "224.0.0.0"; Total = 536870912; Usable = 536870910}
        @{MaskLength = 4; Mask = "240.0.0.0"; Total = 268435456; Usable = 268435454}
        @{MaskLength = 5; Mask = "248.0.0.0"; Total = 134217728; Usable = 134217726}
        @{MaskLength = 6; Mask = "252.0.0.0"; Total = 67108864; Usable = 67108862}
        @{MaskLength = 7; Mask = "254.0.0.0"; Total = 33554432; Usable = 33554430}
        @{MaskLength = 8; Mask = "255.0.0.0"; Total = 16777216; Usable = 16777214}
        @{MaskLength = 9; Mask = "255.128.0.0"; Total = 8388608; Usable = 8388606}
        @{MaskLength = 10; Mask = "255.192.0.0"; Total = 4194304; Usable = 4194302}
        @{MaskLength = 11; Mask = "255.224.0.0"; Total = 2097152; Usable = 2097150}
        @{MaskLength = 12; Mask = "255.240.0.0"; Total = 1048576; Usable = 1048574}
        @{MaskLength = 13; Mask = "255.248.0.0"; Total = 524288; Usable = 524286}
        @{MaskLength = 14; Mask = "255.252.0.0"; Total = 262144; Usable = 262142}
        @{MaskLength = 15; Mask = "255.254.0.0"; Total = 131072; Usable = 131070}
        @{MaskLength = 16; Mask = "255.255.0.0"; Total = 65536; Usable = 65534}
        @{MaskLength = 17; Mask = "255.255.128.0"; Total = 32768; Usable = 32766}
        @{MaskLength = 18; Mask = "255.255.192.0"; Total = 16384; Usable = 16382}
        @{MaskLength = 19; Mask = "255.255.224.0"; Total = 8192; Usable = 8190}
        @{MaskLength = 20; Mask = "255.255.240.0"; Total = 4096; Usable = 4094}
        @{MaskLength = 21; Mask = "255.255.248.0"; Total = 2048; Usable = 2046}
        @{MaskLength = 22; Mask = "255.255.252.0"; Total = 1024; Usable = 1022}
        @{MaskLength = 23; Mask = "255.255.254.0"; Total = 512; Usable = 510}
        @{MaskLength = 24; Mask = "255.255.255.0"; Total = 256; Usable = 254}
        @{MaskLength = 25; Mask = "255.255.255.128"; Total = 128; Usable = 126}
        @{MaskLength = 26; Mask = "255.255.255.192"; Total = 64; Usable = 62}
        @{MaskLength = 27; Mask = "255.255.255.224"; Total = 32; Usable = 30}
        @{MaskLength = 28; Mask = "255.255.255.240"; Total = 16; Usable = 14}
        @{MaskLength = 29; Mask = "255.255.255.248"; Total = 8; Usable = 6}
        @{MaskLength = 30; Mask = "255.255.255.252"; Total = 4; Usable = 2}
        @{MaskLength = 31; Mask = "255.255.255.254"; Total = 2; Usable = 0}
        @{MaskLength = 32; Mask = "255.255.255.255"; Total = 1; Usable = 0}
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $ExpectedParameterList = @(
            'Mask','MaskLength','Verbose','Usable'
            'Debug','ErrorAction','WarningAction','InformationAction',
            'ErrorVariable','WarningVariable','InformationVariable',
            'OutVariable','OutBuffer','PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            it "has the correct default parameter set" {
                $Function.DefaultParameterSet | Should Be 'Mask'
            }
        }

        Context "ParameterSet: Mask" {
            $ParameterSetName = 'Mask'
            $ExpectedParameterSetParams = @(
                'Mask','Verbose','Debug','ErrorAction','WarningAction','Usable',
                'InformationAction','ErrorVariable','WarningVariable',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<Mask>: <Usable>/<Total>)" -TestCases $TestTable {
                param([ipaddress]$Mask,[int]$MaskLength,[int64]$Total,[int64]$Usable)
                {GetMaxAddressCount -Mask $Mask} | Should Not Throw
                GetMaxAddressCount -Mask $Mask | Should Be $Total
                GetMaxAddressCount -Mask $Mask -Usable | Should Be $Usable
            }
        }
        Context "ParameterSet: MaskLength" {
            $ParameterSetName = 'MaskLength'
            $ExpectedParameterSetParams = @(
                'MaskLength','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable','Usable',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<MaskLength>: <Usable>/<Total>)" -TestCases $TestTable {
                param([ipaddress]$Mask,[int]$MaskLength,[int64]$Total,[int64]$Usable)
                {GetMaxAddressCount -MaskLength $MaskLength} | Should Not Throw
                GetMaxAddressCount -MaskLength $MaskLength | Should Be $Total
                GetMaxAddressCount -MaskLength $MaskLength -Usable | Should Be $Usable
            }
        }
    }
}