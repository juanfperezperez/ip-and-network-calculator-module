Param(
    [string]$ModuleName
)

$TestsDirectory = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$SourceDirectory = Join-Path -Path $TestsDirectory -ChildPath ..\source -Resolve
$FunctionName = (Get-Item -Path $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName) -replace '.Tests{0,1}$'

if(($null -eq $ModuleName) -or ([string]::Empty -eq $ModuleName)){
    $ModuleName = If(([array](Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File)).Count -ne 1){ 'UNKNOWN' }
                else{(Get-ChildItem -Path $SourceDirectory -Filter '*.psd1' -File).BaseName}
}

$ModulePath = Join-Path -Path $SourceDirectory -ChildPath "$ModuleName.psd1"

If(($ModuleName -eq 'UNKNOWN') -or (-not (Test-Path -Path $ModulePath -PathType Leaf))){
    Describe $FunctionName {
        it "module name is not 'UNKNOWN'" {
            $ModuleName | Should Not Be 'UNKNOWN'
        }
        it "module manifest exists" {
            $ModulePath | Should Exist
        }
    }
    break;
}

Import-Module -Name $ModulePath -Force


InModuleScope -ModuleName $ModuleName {
    $FunctionName = 'TestNetworkContainsIP'
    $TestTable = @(
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.227.21"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.7.159"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.45.111"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.156.20"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.162.173"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.93.100"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.117.65"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.233.43"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.120.116"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "156.25.35.187"; InRange = $true}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "115.216.15.65"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "193.231.209.73"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "50.41.180.75"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "140.86.39.73"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "107.157.67.14"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "250.48.162.29"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "153.74.80.106"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "131.102.224.100"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "251.247.208.132"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "217.65.206.1"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "2.119.200.185"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "65.54.207.175"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "162.168.170.171"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "85.228.74.209"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "215.178.97.236"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "12.66.50.201"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "170.188.98.200"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "200.96.254.202"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "73.217.192.198"; InRange = $False}
        @{Length = 16; Mask = "255.255.0.0"; IPAddress = "156.25.0.0"; Child = "223.195.63.168"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.193.202.22"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.99.46.130"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.216.57.41"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.193.201.156"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.140.59.92"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.99.196.181"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.207.152.225"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.77.168.10"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.2.130.180"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "84.123.231.58"; InRange = $true}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "30.224.50.41"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "209.250.131.60"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "39.173.189.46"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "197.204.184.49"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "35.62.122.71"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "57.168.84.26"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "219.203.83.67"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "62.22.44.1"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "82.133.28.71"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "210.85.11.78"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "79.132.213.179"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "224.169.202.95"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "11.42.210.243"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "146.14.144.89"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "146.211.149.174"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "159.90.113.129"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "118.216.129.133"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "112.61.106.214"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "122.92.40.212"; InRange = $False}
        @{Length = 8; Mask = "255.0.0.0"; IPAddress = "84.0.0.0"; Child = "80.91.201.188"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.73.39"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.72.200"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.72.206"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.72.131"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.73.88"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.76.65"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.78.47"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.75.151"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.77.200"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "144.124.72.106"; InRange = $true}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "174.102.98.90"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "233.121.12.111"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "92.207.213.128"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "161.168.43.20"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "245.151.227.120"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "64.239.84.60"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "223.90.113.69"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "141.134.25.7"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "21.221.160.92"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "141.59.27.4"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "240.207.162.233"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "122.204.153.237"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "159.78.204.202"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "1.122.202.154"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "198.176.45.224"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "211.96.117.250"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "22.130.139.164"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "16.230.120.241"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "45.205.108.201"; InRange = $False}
        @{Length = 21; Mask = "255.255.248.0"; IPAddress = "144.124.72.0"; Child = "53.106.242.251"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.223.0"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.223.31"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.222.215"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.223.195"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.220.90"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.221.208"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.222.194"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.220.18"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.223.158"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "213.211.221.187"; InRange = $true}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "32.55.104.4"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "48.51.137.67"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "252.242.207.75"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "5.192.141.194"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "181.114.47.46"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "214.26.29.207"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "58.11.79.158"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "86.77.140.210"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "196.216.249.107"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "62.95.67.20"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "37.134.207.227"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "139.139.175.239"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "133.232.243.244"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "93.169.17.229"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "134.35.67.220"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "69.7.122.217"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "132.1.84.233"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "66.197.229.224"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "133.214.218.238"; InRange = $False}
        @{Length = 22; Mask = "255.255.252.0"; IPAddress = "213.211.220.0"; Child = "220.194.120.225"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.79.101"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.3.24"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.36.70"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.4.238"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.51.105"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.102.42"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.53.85"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.69.52"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.103.215"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "147.138.38.16"; InRange = $true}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "210.1.68.95"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "34.159.97.35"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "111.156.124.63"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "217.163.182.67"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "87.196.185.79"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "193.34.102.130"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "101.123.172.145"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "150.21.200.122"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "18.185.62.78"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "8.51.106.128"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "73.182.101.226"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "137.236.37.199"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "143.178.173.157"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "194.27.49.214"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "207.225.41.192"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "197.165.72.198"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "163.211.156.252"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "66.174.133.236"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "89.1.20.189"; InRange = $False}
        @{Length = 17; Mask = "255.255.128.0"; IPAddress = "147.138.0.0"; Child = "115.93.57.169"; InRange = $False}
    )
    Describe $FunctionName {
        $Function = Get-Command -Name $FunctionName
        $ExpectedParameterList = @(
            'IPAddress','Mask','MaskLength','Verbose','ChildIPAddress',
            'Debug','ErrorAction','WarningAction','InformationAction',
            'ErrorVariable','WarningVariable','InformationVariable',
            'OutVariable','OutBuffer','PipelineVariable'
        ) | ForEach-Object {@{'Parameter'=$_}}
        
        Context "Help" {
            $helpObject = Get-Help -Name $FunctionName
            # it "has a synopsis" {
            #     $helpObject.Synopsis | Should Not BeNullOrEmpty
            # }
            # it "has a description" {
            #     $helpObject.Description | Should Not BeNullOrEmpty
            # }
            # it "has at least 1 example" {
            #     $helpObject.Examples | Should Not BeNullOrEmpty
            #     $helpObject.Examples.Example | Should Not BeNullOrEmpty
            #     ([array]($helpObject.Examples.Example)).Count | Should BeGreaterThan 0
            # }
            it "lists inputs" {
                $helpObject.inputTypes | Should Not BeNullOrEmpty
                $helpObject.inputTypes.inputType | Should Not BeNullOrEmpty
                ([array]($helpObject.inputTypes.inputType)).Count | Should BeGreaterThan 0
            }
            it "lists outputs" {
                $helpObject.returnValues | Should Not BeNullOrEmpty
                $helpObject.returnValues.returnValue | Should Not BeNullOrEmpty
                ([array]($helpObject.returnValues.returnValue)).Count | Should BeGreaterThan 0
            }

            # other checks: notes, component, role, functionality
        }

        Context "Structure" {
                       
            it "accepts the <Parameter> parameter" -TestCases $ExpectedParameterList {
                param($Parameter)
                $Parameter | Should BeIn $Function.Parameters.Keys
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $Function.Parameters.Keys | Where-Object {$_ -notin $ExpectedParameterList.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }
            
            it "has the correct default parameter set" {
                $Function.DefaultParameterSet | Should Be 'Mask'
            }
        }

        Context "ParameterSet: Mask" {
            $ParameterSetName = 'Mask'
            $ExpectedParameterSetParams = @(
                'IPAddress','Mask','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable','ChildIPAddress',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<IPAddress>/<Mask> contains <Child>: <InRange>)" -TestCases $TestTable {
                param([int]$Length,[ipaddress]$Mask,[ipaddress]$IPAddress,[ipaddress]$Child,[bool]$InRange)
                {TestNetworkContainsIP -IPAddress $IPAddress -Mask $Mask -ChildIPAddress $Child} | Should Not Throw
                TestNetworkContainsIP -IPAddress $IPAddress -Mask $Mask -ChildIPAddress $Child | Should Be $InRange
            }
        }
        Context "ParameterSet: MaskLength" {
            $ParameterSetName = 'MaskLength'
            $ExpectedParameterSetParams = @(
                'IPAddress','MaskLength','Verbose','Debug','ErrorAction','WarningAction',
                'InformationAction','ErrorVariable','WarningVariable','ChildIPAddress',
                'InformationVariable','OutVariable','OutBuffer','PipelineVariable'
            ) | ForEach-Object {@{'Parameter'=$_}}
            $ParameterSet = $Function.ParameterSets | Where-Object {
                $_.Name -eq $ParameterSetName
            }

            it "accepts the <Parameter> parameter" -TestCases ($ExpectedParameterSetParams) {
                param($Parameter)
                $Parameter | Should BeIn $ParameterSet.Parameters.Name
            }
            
            it "Has no unexpected parameters" {
                [array]$UnexpectedParameters = $ParameterSet.Parameters.Name | Where-Object {$_ -notin $ExpectedParameterSetParams.Values}
                $UnexpectedParameters | Should BeNullOrEmpty
            }

            it "executes with correct data (<IPAddress>/<Length> contains <Child>: <InRange>)" -TestCases $TestTable {
                param([int]$Length,[ipaddress]$Mask,[ipaddress]$IPAddress,[ipaddress]$Child,[bool]$InRange)
                {TestNetworkContainsIP -IPAddress $IPAddress -MaskLength $Length -ChildIPAddress $Child} | Should Not Throw
                TestNetworkContainsIP -IPAddress $IPAddress -MaskLength $Length -ChildIPAddress $Child | Should Be $InRange
            }
        }
    }
}