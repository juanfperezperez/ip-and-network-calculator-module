$Script:ModuleName = 'IPAndNetworkCalculator'

$Script:RootDir = Get-Location | Select-Object -ExpandProperty ProviderPath

$Script:moduleSource    = [System.IO.Path]::Combine([string[]]@($RootDir, 'source'))
$Script:BuildDir        = [System.IO.Path]::Combine([string[]]@($RootDir, '.build'))
$Script:PublishPath     = [System.IO.Path]::Combine([string[]]@($RootDir, '.publish'))
$Script:ModuleBuildDir  = [System.IO.Path]::Combine([string[]]@($BuildDir, 'Module'))
$Script:ModuleBuildPath = [System.IO.Path]::Combine([string[]]@($ModuleBuildDir, $ModuleName))

$Script:ModuleInfo = Get-Module -ListAvailable -Name $ModuleBuildPath
$Script:ModuleZipPackagePath = [System.IO.Path]::Combine([string[]]@($PublishPath, "$($ModuleName).$($ModuleInfo.Version.ToString()).zip"))
$Script:ArchiveScript = [System.IO.Path]::Combine([string[]]@($RootDir, "Archive.PSDeployment.ps1"))


# test repository
$Script:VolatileRepoName = '{0}_{1}' -f "Local",(New-Guid).ToString().Substring(0,7)

$RegisterVolatileRepo = {
    $VolatileRepoParams = @{
        "Name" = $Script:VolatileRepoName
        "SourceLocation" = $PublishPath #Resolve-Path -Path ".\.publish" | Select-Object -ExpandProperty ProviderPath
        "PublishLocation" = $PublishPath #Resolve-Path -Path ".\.publish" | Select-Object -ExpandProperty ProviderPath
        "InstallationPolicy" = "Untrusted"
        "PackageManagementProvider" = "NuGet"
    }
    if(! (Test-Path -Path $PublishPath -PathType Container)){New-Item -Path $PublishPath -ItemType Directory | Out-Null}
    Register-PSRepository @VolatileRepoParams
}

$UnRegisterVolatileRepo = {
    Unregister-PSRepository -Name $Script:VolatileRepoName
}

Deploy Module {
    By PSGalleryModule NugetToLocalRepo {
        FromSource $ModuleBuildPath
        To $Script:VolatileRepoName
        WithPreScript $RegisterVolatileRepo
        WithPostScript $UnRegisterVolatileRepo
        Tagged "NuGet"
    }
    By Task PackageZip {
        FromSource $Script:ArchiveScript
        Tagged "PackageZip"
        WithOptions @{
            'SourcePath' = $Script:ModuleBuildPath
            'DestinationPath' = $Script:ModuleZipPackagePath
        }
    }
    # By PSGalleryModule PSGallery {
    #     FromSource $ModuleBuildPath
    #     To "PSGallery"
    #     Tagged "PSGallery"
    # }
}