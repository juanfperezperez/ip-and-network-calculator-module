---
external help file: IPAndNetworkCalculator-help.xml
Module Name: IPAndNetworkCalculator
online version: 
schema: 2.0.0
---

# New-IPCalcNetwork

## SYNOPSIS
New-IPCalcNetwork returns an object that represents a network.

## SYNTAX

### NetworkMask (Default)
```
New-IPCalcNetwork [-NetworkID] <String> [-NetworkMask] <String>
```

### NetworkMaskLength
```
New-IPCalcNetwork [-NetworkID] <String> [-NetworkMaskLength] <Int32>
```

## DESCRIPTION
New-IPCalcNetwork returns an object that represents a network.
The resulting object will
contain the IP address identifying the network (AKA the subnet ID), the subnet
mask for the network, and the mask length (AKA CIDR suffix).

## EXAMPLES

### -------------------------- EXAMPLE 1 --------------------------
```
PS> New-IPCalcNetwork -NetworkID 192.168.1.0 -NetworkMask 255.255.255.0


ID               : 192.168.1.0
MaskLength       : 24
Mask             : 255.255.255.0
BroadcastAddress : 192.168.1.255
AddressClass     : C
IsPrivate        : True
MaxAddressCount  : 256
MaxHostCount     : 254
MaxSubnetCount   : 64
FirstHost        : 192.168.1.1
LastHost         : 192.168.1.254
```

Returns an object representing the new network with the provided IP address
and Mask

### -------------------------- EXAMPLE 2 --------------------------
```
PS> New-IPCalcNetwork -NetworkID 192.168.1.0 -NetworkMaskLength 24


ID               : 192.168.1.0
MaskLength       : 24
Mask             : 255.255.255.0
BroadcastAddress : 192.168.1.255
AddressClass     : C
IsPrivate        : True
MaxAddressCount  : 256
MaxHostCount     : 254
MaxSubnetCount   : 64
FirstHost        : 192.168.1.1
LastHost         : 192.168.1.254
```

Returns an object representing the new network with the provided IP address
and Mask of the specified length

## PARAMETERS

### -NetworkID
IP address string in dotted decimal notation (0.0.0.0) of an IP address in the
network to be returned. If this is not the actual network ID, the function will
calculate the correct IP and return it as part of the resulting object.

```yaml
Type: System.String
Parameter Sets: (All)
Aliases: 

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -NetworkMask
Network mask string in dotted decimal notation (0.0.0.0) of the network to be
returned. If this is not a valid subnet mask, the function will error.

```yaml
Type: System.String
Parameter Sets: NetworkMask
Aliases: 

Required: True
Position: 2
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -NetworkMaskLength
Integer in the range 0 - 32 representing the mask length of the network to be
returned.

```yaml
Type: System.Int32
Parameter Sets: NetworkMaskLength
Aliases: 

Required: True
Position: 2
Default value: 0
Accept pipeline input: False
Accept wildcard characters: False
```

## INPUTS

### System.String

System.Int32

## OUTPUTS

### System.Management.Automation.PSCustomObject

## NOTES
This function is intended to be used as a tool for network planning.
It should help planners quickly split networks and determine if a particular IP
address belongs to that network.

<!-- ## RELATED LINKS

[New-IPCalcIPAddress](./New-IPCalcIPAddress.md) -->