# New-IPCalcIPAddress

## SYNOPSIS
New-IPCalcIPAddress returns an IPAddress object for the corresponding input.

## SYNTAX

### String (Default)
```
New-IPCalcIPAddress [-IPAddressString] <String>
```

### ByteArr
```
New-IPCalcIPAddress [-IPAddressBytes] <Byte[]>
```

## DESCRIPTION
New-IPCalcIPAddress returns an IPAddress object for the corresponding input.
It accepts an IP address string in dotted decimal notation (0.0.0.0) or an array
of four bytes (\[byte\[\]\]@(0,0,0,0)).

## EXAMPLES

### -------------------------- EXAMPLE 1 --------------------------
```
New-IPCalcIPAddress -IPAddressString 192.168.0.1

Address            : 16820416
AddressFamily      : InterNetwork
ScopeId            :
IsIPv6Multicast    : False
IsIPv6LinkLocal    : False
IsIPv6SiteLocal    : False
IsIPv6Teredo       : False
IsIPv4MappedToIPv6 : False
IPAddressToString  : 192.168.0.1
```
### -------------------------- EXAMPLE 2 --------------------------
```
New-IPCalcIPAddress -IPAddressBytes [byte[]]@(192,168,0,1)

Address            : 16820416
AddressFamily      : InterNetwork
ScopeId            :
IsIPv6Multicast    : False
IsIPv6LinkLocal    : False
IsIPv6SiteLocal    : False
IsIPv6Teredo       : False
IsIPv4MappedToIPv6 : False
IPAddressToString  : 192.168.0.1
```
## PARAMETERS

### -IPAddressString
IP address string in dotted decimal notation (0.0.0.0)

```yaml
Type: System.String
Parameter Sets: String
Aliases: 

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -IPAddressBytes
Array of four bytes (\[byte\[\]\]@(0,0,0,0))

```yaml
Type: System.Byte[]
Parameter Sets: ByteArr
Aliases: 

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

## INPUTS

### System.String

System.Byte[]

## OUTPUTS

### System.Net.IPAddress

## NOTES
This function is a wrapper around the System.Net.IPAddress class constructor
and limits it to the creation of IPv4 Addresses only.

## RELATED LINKS

[New-IPCalcNetwork](./New-IPCalcNetwork.md)